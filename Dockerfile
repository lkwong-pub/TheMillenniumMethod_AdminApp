FROM java:8
EXPOSE 8080

ADD app/build/libs/app-0.1.jar mm-admin.jar

ENTRYPOINT ["java", "-jar", "mm-admin.jar"]

# Delete existing ROOT folder
# ADD base/docker_conf/server.xml /usr/local/tomcat/conf/server.xml
# RUN rm -rf /usr/local/tomcat/webapps/ROOT/WEB-INF/
# RUN rm -rf /usr/local/tomcat/webapps/ROOT/

# ADD app/build/libs/app-0.1.war /usr/local/tomcat/webapps/ROOT.war

# CMD ["catalina.sh", "run"]