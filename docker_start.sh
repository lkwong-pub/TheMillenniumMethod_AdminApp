docker stop millennium-method-admin
docker rm millennium-method-admin
docker run \
-p 8000:8080 \
-e CATALINA_OPTS="\
-Dspring.profiles.active=uat \
" \
--name millennium-method-admin \
--link mysql:mysql \
--link minio:minio \
--link millennium-method-resources:millennium-method-resources \
--link redis:redis \
millennium-method/admin:latest