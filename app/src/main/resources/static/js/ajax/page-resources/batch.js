// jQuery
function replaceItems(html) {
    // Replace the <fieldset id="items"> with a new one returned by server.
    $('#items').replaceWith($(html));
    $.AdminBSB.select.activate();


    $(".ckeditor").each(function (index) {
        // console.log($(this).attr("name"))

        // CKEDITOR.config.allowedContent = true;
        // Disable button

        CKEDITOR.replace($(this).attr("name"), {
            language: "en",
            allowedContent: true,
            // removeButtons: "Form, Checkbox, Radio, TextField, Textarea, Select, Button, ImageButton, HiddenField",
            removeButtons: "Save",
            on: {
                instanceReady: function () {
                    this.document.appendStyleSheet('/css/userguide.css');
                }
            }
        });
    });
    $('html,body').animate({scrollTop: $("#addItem").offset().top});

}


// $(function() {
//
//     $('.addItem').on('click', function () {
//         saveEditorTrigger();
//         // event.preventDefault();
//         var data = $('form').serialize();
//         // Add parameter "addItem" to POSTed form data. Button's name and value is
//         // POSTed only when clicked. Since "event.preventDefault();" prevents from
//         // actual clicking the button, following line will add parameter to form
//         // data.
//         // data += 'addItem';
//         $.post('/page-resources/add/item', data, replaceItems); // Backend returned View for replace
//         //
//     });
// });

$(document).on('click', '.addItem', function (event) {

    saveEditorTrigger();
    event.preventDefault();
    var data = $('form').serialize();
    $.post('/page-resources/add/item', data, replaceItems); // Backend returned View for replace

});

$(document).on('click', '.btnDelete', function (event) {

    saveEditorTrigger();
    event.preventDefault();
    var data = $('form').serialize();
    var index = $(this).data("index");
    console.log(index)
    // Add parameter and index of item that is going to be removed.
    // data += 'removeItem=' + $(this).val();
    $.post('/page-resources/remove/item/' + index, data, replaceItems);
});

$(document).on('click', '.batchDelete', function (event) {

    event.preventDefault();
    var idList = [];
    $.each($(".listItemCheck:checked"), function () {
        idList.push($(this).data("index"));
    });
    var test = JSON.stringify(idList);
    console.log(idList)
    console.log(test)
    // $.post('/page-resources/delete/batch', test);

    swal({
        title: "Are you sure?",
        text: "You will not be able to recover those records!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {

        $.ajax({
            type: "POST",
            url: "/page-resources/delete/batch",
            // The key needs to match your method's input parameter (case-sensitive).
            data: JSON.stringify(idList),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                console.log(data.msg);
                swal({
                        title: "Deleted!",
                        text: "Selected records has been deleted.",
                        type: "success"
                    },
                    function () {
                        getPageContent("/page-resources/list");
                        // location.reload()
                        // window.location.href = 'login.html';
                    });
                // swal("Deleted!", "Selected records has been deleted.", "success");

            },
            error: function (data) {
                // console.log(data);
                // console.log(data.responseJSON);
                // console.log(data.responseJSON.msg);
                swal(data.responseJSON.msg);
            }
        });
    });

});
// $('button[name="addItem"]').click(function (event) {

function saveEditorTrigger() {

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }

    // for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
}