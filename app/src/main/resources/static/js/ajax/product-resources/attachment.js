$(document).on('click', '.upload-attachment', function (event) {

    event.preventDefault();
    var url = $(this).closest('form').attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data:  new FormData(this),
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function()
        {
            loginSessionCheck(xhr)
        },
        success: function (response, textStatus, xhr) {
            loginSessionCheck(xhr);
            replaceElements(response);
            msgBox('success', 'Upload success', 100);

        },
        error: function (e) {
            msgBox('danger', e, 100);
            console.log("Server error - " + e);
        }
    });

});