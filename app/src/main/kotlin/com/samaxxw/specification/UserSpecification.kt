package com.samaxxw.specification

import com.samaxxw.model.User
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

class UserSpecification : Specification<User> {

    val criteria: SearchCriteria? = null

    override fun toPredicate(root: Root<User>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {
//        if (criteria!!.operation.equals(">", ignoreCase = true)) {
//                        return builder.greaterThanOrEqualTo(
//                          root.<String> get(criteria.getKey()), criteria.getValue().toString())
//                    
//        }
//                else if (criteria.getOperation().equalsIgnoreCase("<")) {
//                        return builder.lessThanOrEqualTo(
//                          root.<String> get(criteria.getKey()), criteria.getValue().toString());
//                    
//        }
//                else if (criteria.getOperation().equalsIgnoreCase(":")) {
//                        if (root.get(criteria.getKey()).getJavaType() == String.class) {
//                            return builder.like(
//                              root.<String>get(criteria.getKey()), "%"+criteria.getValue()+"%");
//                        
//        } else {
//                            return builder.equal(root.get(criteria.getKey()), criteria.getValue());
//                        
//        }
//                    
//        }
//                return null;

        if (criteria!!.operation.equals(">", ignoreCase = true)) {
            return criteriaBuilder.greaterThanOrEqualTo(
                    root.get<String>(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equals("<", ignoreCase = true)) {
            return criteriaBuilder.lessThanOrEqualTo(
                    root.get<String>(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equals(":",  ignoreCase = true )) {
            return if (root.get<String>(criteria.key).javaType === String::class.java) {
                criteriaBuilder.like(
                        root.get<String>(criteria.key), "%" + criteria.value + "%")
            } else {
                criteriaBuilder.equal(root.get<String>(criteria.key), criteria.value)
            }
        }
        return null
    }


}
