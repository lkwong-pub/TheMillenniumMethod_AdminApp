package com.samaxxw.specification

class SearchCriteria {
    val key: String? = null
    val operation: String? = null
    val value: Any? = null
}