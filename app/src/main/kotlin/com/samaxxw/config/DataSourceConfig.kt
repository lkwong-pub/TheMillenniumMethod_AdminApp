package com.samaxxw.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = ["com.samaxxw.repository"],
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager"
)
@EnableMongoRepositories(basePackages = ["com.samaxxw.mongo.repository"])
class DataSourceConfig {

    @ConfigurationProperties(prefix = "spring.datasource.hikari")
    @Bean
    fun datasource(): DataSource =
            DataSourceBuilder.create().build()

        @Bean("entityManagerFactory")
    fun entityManagerFactory(_builder: EntityManagerFactoryBuilder): LocalContainerEntityManagerFactoryBean {
        return _builder.dataSource(datasource())
                .persistenceUnit("default-PU")
                .packages("com.samaxxw.model", "com.samaxxw.service")
                .build()
    }


    @Bean("transactionManager")
    fun transactionManager(_entityManagerFactory: EntityManagerFactory): PlatformTransactionManager {
        return JpaTransactionManager().apply {
            entityManagerFactory = _entityManagerFactory
        }
    }

}