package com.samaxxw.config

import org.springframework.context.annotation.Bean
import org.springframework.session.data.redis.config.ConfigureRedisAction
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession


@EnableRedisHttpSession(redisNamespace = "spring:session:millennium-admin")
class RedisConfig {

//    @Bean
//    fun configureRedisAction(): ConfigureRedisAction {
//        return ConfigureRedisAction.NO_OP
//    }

}


//@Configuration
//@EnableCaching
//class RedisConfig {
//
//    @Value("\${redis.hostname}")
//    private val redisHostName: String? = null
//
//    @Value("\${redis.port}")
//    private val redisPort: Int = 0
//
//    @Value("\${redis.prefix}")
//    private val redisPrefix: String? = null
//
//    @Bean
//    internal fun jedisConnectionFactory(): JedisConnectionFactory {
//        val redisStandaloneConfiguration = RedisStandaloneConfiguration(redisHostName!!, redisPort)
//        return JedisConnectionFactory(redisStandaloneConfiguration)
//    }
//
//    @Bean(value = "redisTemplate")
//    fun redisTemplate(redisConnectionFactory: RedisConnectionFactory): RedisTemplate<String, Any> {
//        val redisTemplate = RedisTemplate<String, Any>()
//        redisTemplate.setConnectionFactory(redisConnectionFactory)
//        return redisTemplate
//    }
//
//    @Primary
//    @Bean(name = ["cacheManager"]) // Default cache manager is infinite
//    fun cacheManager(redisConnectionFactory: RedisConnectionFactory): CacheManager {
//        return RedisCacheManager.builder(redisConnectionFactory).cacheDefaults(RedisCacheConfiguration.defaultCacheConfig().prefixKeysWith(redisPrefix!!)).build()
//    }
//
//    @Bean(name = ["cacheManager1Hour"])
//    fun cacheManager1Hour(redisConnectionFactory: RedisConnectionFactory): CacheManager {
//        val expiration = Duration.ofHours(1)
//        return RedisCacheManager.builder(redisConnectionFactory)
//                .cacheDefaults(RedisCacheConfiguration.defaultCacheConfig().prefixKeysWith(redisPrefix!!).entryTtl(expiration)).build()
//    }
//}