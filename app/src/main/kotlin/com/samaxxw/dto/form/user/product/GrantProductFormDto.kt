package com.samaxxw.dto.form.user.product

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class GrantProductFormDto {

    @NotNull
    var productId: Long? = null
    @NotNull
    var userId: Long? = null

    @NotBlank
    var remarks: String? = null

}