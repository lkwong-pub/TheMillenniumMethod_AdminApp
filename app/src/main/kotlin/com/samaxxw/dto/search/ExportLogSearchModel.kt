package com.samaxxw.dto.search

import com.samaxxw.util.ExportTypeEnum
import java.io.Serializable

class ExportLogSearchModel : Serializable {

    var fromDate : String? = null
    var toDate : String? = null
    var exportType : ExportTypeEnum? = null

}