package com.samaxxw.controller

import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.service.impl.PageResourcesExportService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.time.LocalDateTime
import java.time.ZoneOffset

@Controller
@RequestMapping(value = [SystemAdminController.ROOT])
class SystemAdminController : AbstractRestHandler() {


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/admin"
    }



    @GetMapping("/index")
    fun indexPage(model: Model) : String {
//        indexModelAttribute(model)
        return getViewPath("admin/index", request!!)
    }

    @GetMapping("/export")
    fun exportRecordPage() : String {
        // Add export type
        return getViewPath("admin/export", request!!)
    }

    
}
