package com.samaxxw.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import java.security.Principal

@Controller
class UITestController : AbstractRestHandler() {

    @RequestMapping("/ui_test")
    fun uiTest(model: Model) : String {
        val principal : Principal = request!!.userPrincipal
//        print(principal.name)
        log.info(principal.name)
        model.addAttribute("timeoutTest", "/test/timeout")
        model.addAttribute("nontimeoutTest", "/test/nontimeout")

        return getViewPath("base/ui_test", request)
    }

    @RequestMapping("/test/timeout")
    fun timeoutTest() : String {
//        print(principal.name)
        Thread.sleep(10_000)  // wait for 1 second
        return getViewPath("base/ui_test", request!!)

    }

    @RequestMapping("/test/nontimeout")
    fun nonTimeoutTest() : String {
//        print(principal.name)
        Thread.sleep(2_000)  // wait for 1 second
        return getViewPath("base/ui_test", request!!)

    }
    
}
