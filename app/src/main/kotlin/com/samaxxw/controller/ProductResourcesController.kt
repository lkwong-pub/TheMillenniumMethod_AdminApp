package com.samaxxw.controller

import com.samaxxw.controller.ProductResourcesController.Companion.ROOT
import com.samaxxw.controller.searchModel.ProductResourcesSearchModel
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_PRODUCT_RESOURCES
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.create.CreateProductResourceDto
import com.samaxxw.dto.create.UpdateProductResourceDto
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.form.product.ProductEditFormDto
import com.samaxxw.dto.form.product.ProductResourcesAttachmentForm
import com.samaxxw.dto.form.product.ProductResourcesEditFormDto
import com.samaxxw.dto.form.product.ProductResourcesFormDto
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dto.model.ProductResourceDto
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.ProductResourcesDtoService
import com.samaxxw.exception.ValidationException
import com.samaxxw.model.ProductResource
import com.samaxxw.service.ProductResourcesService
import com.samaxxw.service.ProductService
import com.samaxxw.service.ui.BreadCrumbService
import com.samaxxw.util.BaseStatusEnum
import com.samaxxw.util.ProductResourcesStatusEnum
import com.samaxxw.util.ResourcesTypeEnum
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.util.FileCopyUtils
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.thymeleaf.util.StringUtils
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
@RequestMapping(value = [ROOT])
class ProductResourcesController : AdvancedBaseRestController<ProductResourceDto, CreateProductResourceDto, UpdateProductResourceDto>() {

    @Autowired
    var productResourcesDtoService: ProductResourcesDtoService? = null

    @Autowired
    var productService: ProductService? = null

    @Autowired
    var productResourcesService: ProductResourcesService? = null

    @Autowired
    var breadCrumbService: BreadCrumbService? = null


    override val dtoService: BaseDtoService<ProductResourceDto>
        get() = productResourcesDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/product-resources"
        const val BINDRESULT_PRODUCTRESOURCESEDITFORM_KEY = "org.springframework.validation.BindingResult.productResourcesEditForm"

    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_PRODUCT_RESOURCES)

        // TODO sub menu
        model.addAttribute(NAV_KEY, NavKeyConstant.NAV_PRODUCT)

//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Product Category List

    }

    fun listViewModelAttribute(page: Int?, per_page: Int?, sortField: String, direction: String, response: HttpServletResponse, model: Model, productId: Long) {


        var productResourcesSearchModel = ProductResourcesSearchModel()
        productResourcesSearchModel.productId = productId
        request!!.session.setAttribute(SEARCH_FILTER_NAME_PRODUCT_RESOURCES, productResourcesSearchModel)

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)

        model.addAttribute("addUrl", "$ROOT/product/$productId/add")
        model.addAttribute("editUrl", "$ROOT/product/$productId/edit")
        model.addAttribute("attachmentUrl", "$ROOT/product/$productId/attachment")

        model.addAttribute(PAGE_HEADER, "Product Resources")

        model.addAttribute("status", ProductResourcesStatusEnum.getDisplayMap())
        model.addAttribute("list", result)

        // return for pagination indicator
        model.addAttribute("pageNum", page)
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductResourcesRootBreadCrumb(productId, true))
    }

    fun editViewModelAttribute(model: Model, productId: Long, id: Long) {

        model.addAttribute(PAGE_HEADER, "Update Product Resource")

        val productResource = productResourcesDtoService?.findById(id)!!

        model.addAttribute("productId", productResource.product!!.id)
        model.addAttribute("productResourcesEditForm", convertEditFormDto(productResource))
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductResourcesEditBreadCrumb(productId))

        if (productResource.type.equals(ResourcesTypeEnum.ATTACHMENT.name)) {
            model.addAttribute("attachment", productResource.fileAttachment)
        }

    }

    fun addViewModelAttribute(model: Model, productId: Long, productResourcesFormDto: ProductResourcesFormDto) {
        model.addAttribute(PAGE_HEADER, "Product Resources Configuration")
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductResourcesAddBreadCrumb(productId))
        productResourcesFormDto.productId = productId
        model.addAttribute("productId", productId)
        model.addAttribute("productResourcesAttachmentForm", ProductResourcesAttachmentForm())
    }

    @RequestMapping(value = ["/product/{productId}/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @PathVariable("productId") productId: Long,
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            productResourcesSearchModel: ProductResourcesSearchModel,
            model: Model,
            response: HttpServletResponse
//            request: HttpServletRequest
    ): String {

        listViewModelAttribute(page, per_page, sortField, direction, response, model, productId.toLong())

        return getViewPath("product/resources/list", request!!)
    }

    @ModelAttribute("productResourcesEditForm")
    fun productResourcesEditFormDto(): ProductResourcesEditFormDto {
        return ProductResourcesEditFormDto()
    }

    @GetMapping(value = ["/attachment/{uploadId}"])
    fun getAttachment(response: HttpServletResponse,
                      @PathVariable uploadId: String,
                      attr: RedirectAttributes): Any {

        val fileMeta = productResourcesService?.getFileInfo(uploadId)
        val fileByte = productResourcesService?.downloadFile(uploadId)
        val resource = ByteArrayResource(fileByte!!)

        if (fileByte.isNotEmpty()) {
//        response.contentType = "application/octet-stream"
//        response.addHeader("Content-Disposition", String.format("inline; filename=${fileMeta!!.fileName}"))
//        response.setContentLength(fileMeta.fileSize!!.toInt())
//        FileCopyUtils.copy(fileByte!!, response.outputStream)

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment;filename=" + StringUtils.trim(fileMeta!!.fileName!!))
                    .contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(fileMeta.fileSize!!)
                    .body(resource)
        } else {

            attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.WARNING, "Attachment not found"))
            // redirect to product resources form (through Controller)
            return "redirect:/message"
        }

    }

    // Delete product resources
    @DeleteMapping(value = ["/product/{productId}/attachment/{id}"])
    fun delete(model: Model,
               @PathVariable("productId") productId: Long,
               @PathVariable("id") id: Long,
               response: HttpServletResponse,
               request: HttpServletRequest): String {

        productResourcesService?.delete(id)

        listViewModelAttribute(DEFAULT_PAGE_NUM.toInt(), DEFAULT_PAGE_SIZE.toInt(), DEFAULT_SORT, DEFAULT_SORT_DIR, response, model, productId)
        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Product Resources Deleted"))

        // TODO redirect to upload attachment page
        return getViewPath("product/resources/list", request)
        // return to attachment list page
    }

    @GetMapping(value = ["/product/{productId}/edit/{id}"])
    fun edit(model: Model, @PathVariable("id") id: Long, @PathVariable("productId") productId: Long): String {
        model.addAttribute(PAGE_HEADER, "Product Resources Configuration")
        editViewModelAttribute(model, productId, id)
        return getViewPath("product/resources/edit", request!!)
    }

    // TODO implement product resources form

    @PostMapping(value = ["/product/{productId}/edit/submit"])
    fun edit(@ModelAttribute("productResourcesEditForm") @Valid productResourcesEditFormDto: ProductResourcesEditFormDto,
             result: BindingResult,
             @PathVariable("productId") productId: Long,
             attr: RedirectAttributes,
             model: Model,
             response: HttpServletResponse,
             request: HttpServletRequest
    ): String {

        val resourceId = productResourcesEditFormDto.id
        if (result.hasErrors()) {
            editViewModelAttribute(model, productId, resourceId!!)
            return getViewPath("product/resources/edit", request)
        }
        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Product Resources Updated"))
        try {
            var productResource = productResourcesService!!.update(productResourcesEditFormDto)
            editViewModelAttribute(model, productId, resourceId!!)
        } catch (ve: ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
        }
        return getViewPath("product/resources/edit", request)
    }

    @GetMapping(value = ["/product/{productId}/add"])
    fun add(model: Model,
            @PathVariable productId: Long,
            @ModelAttribute("productResourcesForm") productResourcesFormDto: ProductResourcesFormDto): String {
        addViewModelAttribute(model, productId, productResourcesFormDto)
        return getViewPath("product/resources/add", request!!)
    }

    @PostMapping(value = ["/product/{productId}/add/submit"])
    fun add(@ModelAttribute("productResourcesForm") @Valid productResourcesFormDto: ProductResourcesFormDto,
            result: BindingResult,  // must be follow the validation form behind
            @PathVariable("productId") productId: Long,
            model: Model,
            attr: RedirectAttributes,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        var createdProduct: ProductResource

        try {
            // Do a form validation check first
            if (result.hasErrors()) {
                addViewModelAttribute(model, productId, productResourcesFormDto)
                return getViewPath("product/resources/add", request)

            }
            createdProduct = productResourcesService!!.save(productResourcesFormDto)
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Product Resources Created"))

        } catch (e: Exception) {
            e.printStackTrace()
            addViewModelAttribute(model, productId, productResourcesFormDto)
            return getViewPath("product/resources/add", request)

        }

        listViewModelAttribute(DEFAULT_PAGE_NUM.toInt(), DEFAULT_PAGE_SIZE.toInt(), DEFAULT_SORT, DEFAULT_SORT_DIR, response, model, productId)

        // TODO redirect to upload attachment page
        return getViewPath("product/resources/list", request!!)

    }

    // Add attachment API
    @PostMapping(value = ["/product/{productId}/attachment-upload/{id}}"])
    fun addAttachment(file: MultipartFile,
                      result: BindingResult,  // must be follow the validation form behind
                      @PathVariable("productId") productId: Long,
                      @PathVariable("id") id: Long,
                      model: Model,
                      response: HttpServletResponse,
                      request: HttpServletRequest
    ): ResponseEntity<Any> {

        try {
            productResourcesService?.uploadAttachment(id, productId, file)

            return ResponseEntity.noContent().build()
        } catch (e: Exception) {
            e.printStackTrace()
            return ResponseEntity.badRequest().body(e.cause.toString())
        }

    }


    fun convertEditFormDto(productResourceDto: ProductResourceDto): ProductResourcesEditFormDto {
        // TODO move to mapper
        val result = ProductResourcesEditFormDto()
        result.id = productResourceDto.id
        result.productId = productResourceDto.product!!.id
        result.type = productResourceDto.type
        result.title = productResourceDto.name
        result.url = productResourceDto.resource
        result.status = productResourceDto.status
        result.description = productResourceDto.description

        return result
    }
}