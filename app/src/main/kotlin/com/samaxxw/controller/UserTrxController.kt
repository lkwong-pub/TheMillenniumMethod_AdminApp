package com.samaxxw.controller

import com.samaxxw.controller.UserTrxController.Companion.ROOT
import com.samaxxw.controller.searchModel.SalesTrxSearchModel
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_SALES_TRX
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_USER
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.create.CreateSalesTrxDto
import com.samaxxw.dto.create.UpdateSalesTrxDto
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.form.trx.SalesTrxStatusUpdateRequest
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dto.model.SalesTrxDto
import com.samaxxw.dto.search.BaseSearchRequest
import com.samaxxw.dto.search.SalesTrxLogSearchRequest
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.dtoservice.SalesTrxDtoService
import com.samaxxw.dtoservice.SalesTrxLogDtoService
import com.samaxxw.service.SalesTrxService
import com.samaxxw.service.impl.SalesTrxLogTrxService
import com.samaxxw.service.ui.BreadCrumbService
import com.samaxxw.service.ui.UserPurchaseControllerService
import com.samaxxw.util.SalesStatus
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
@RequestMapping(value = [ROOT])
class UserTrxController : AdvancedBaseRestController<SalesTrxDto, CreateSalesTrxDto, UpdateSalesTrxDto>() {

    @Autowired
    var salesTrxDtoService : SalesTrxDtoService? = null

    @Autowired
    val productDtoService : ProductDtoService? = null

    @Autowired
    val salesTrxService : SalesTrxService? = null

    @Autowired
    var breadCrumbService : BreadCrumbService? = null

    @Autowired
    var salesTrxLogService : SalesTrxLogTrxService? = null

    @Autowired
    var userPurchaseControllerService: UserPurchaseControllerService? = null


    override val dtoService: BaseDtoService<SalesTrxDto>
        get() = salesTrxDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/trx"
//        const val BINDRESULT_USEREDITFORM_KEY = "org.springframework.validation.BindingResult.userEditForm"
        
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_USER)
//        model.addAttribute("url", baseUrl + "/" + ROOT_URL + "/")
//        model.addAttribute("listUrl", baseUrl + "/" + ROOT_URL + "/")
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Role List
        val productList : MutableList<ProductDto> = productDtoService!!.findAll()
        model.addAttribute("productList", productList)

        model.addAttribute(NAV_KEY, NavKeyConstant.NAV_USER) // TODO sub menu

    }

    @ModelAttribute("salesTrxStatusUpdateRequest")
    fun salesTrxStatusUpdateRequest(): SalesTrxStatusUpdateRequest {
        return SalesTrxStatusUpdateRequest()
    }

    fun updateStatusViewModelVal(salesTrxId: Long, model: Model, baseSearchRequest: BaseSearchRequest): Model {
        // get salesTrx
        val salesTrxDto = salesTrxDtoService?.findById(salesTrxId)
        val salesStatus = SalesStatus.getDisplayMap()

        val salesTrxStatusUpdateRequest = SalesTrxStatusUpdateRequest()
        salesTrxStatusUpdateRequest.salesTrxStatus = salesTrxDto?.salesStatus
        salesTrxStatusUpdateRequest.remarks = ""

        // TODO breadcrumb
        model.addAttribute("salesTrx", salesTrxDto)
        model.addAttribute("pageHeader", "Update Transaction Status - ${salesTrxDto!!.trxReferenceCode}")
        model.addAttribute("submitUrl", "$ROOT/$salesTrxId/updateStatus")
        model.addAttribute("salesTrxStatusUpdateRequest", salesTrxStatusUpdateRequest)
        model.addAttribute("salesStatus", salesStatus)
        model.addAttribute(BREABCRUMB, breadCrumbService?.getSalesTrxStatusChangeBreadCrumb(salesTrxDto.user!!.id!!))

        // TODO get trx logs
        model.addAttribute("list", userPurchaseControllerService?.getSalesTrxLog(salesTrxId, baseSearchRequest))

        return model
    }

    @RequestMapping(value = ["/user/{userId}/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @PathVariable("userId") userId: Long,
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            salesTrxSearchModel: SalesTrxSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {


        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, salesTrxSearchModel)
        } else if (request.method == RequestMethod.GET.name) {
            val salesTrxUserFilter = SalesTrxSearchModel()
            salesTrxUserFilter.userId = userId // default filter by user ID
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, salesTrxUserFilter)
        }

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        model.addAttribute(PAGE_HEADER, "User Sales Transaction")
        model.addAttribute("list", result)

        // return for pagination indicator
        model.addAttribute("pageNum", page)

        return getViewPath("user/trx/list", request)

    }

    @RequestMapping(value = ["/{id}/updateStatus"], method = [RequestMethod.GET])
    fun updateStatusView(
            @PathVariable("id") id : Long,
            searchRequest: BaseSearchRequest,
            model: Model) : String {
        updateStatusViewModelVal(id, model, searchRequest)
        return getViewPath("user/trx/update_status", request!!)

    }

    @RequestMapping(value = ["/{id}/updateStatus"], method = [RequestMethod.POST])
    fun updateStatus(
            @PathVariable("id") id : Long,
            @Valid salesTrxStatusUpdateRequest : SalesTrxStatusUpdateRequest,
            model: Model
    ) : String {
        salesTrxService?.updateStatus(id, salesTrxStatusUpdateRequest.salesTrxStatus!!, salesTrxStatusUpdateRequest.remarks!!)
        updateStatusViewModelVal(id, model, BaseSearchRequest())
        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Transaction status updated"))
        return getViewPath("user/trx/update_status", request!!)

    }

}