package com.samaxxw.controller

import com.samaxxw.controller.ProductController.Companion.ROOT
import com.samaxxw.controller.searchModel.ProductSalesTrxSearchModel
import com.samaxxw.controller.searchModel.ProductSearchModel
import com.samaxxw.controller.searchModel.SalesTrxSearchModel
import com.samaxxw.controller.searchModel.SearchModelName
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_PRODUCT
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_PRODUCT_SALES_TRX
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.create.CreateProductDto
import com.samaxxw.dto.create.UpdateProductDto
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.form.product.ProductEditFormDto
import com.samaxxw.dto.form.product.ProductFormDto
import com.samaxxw.dto.form.product.ProductResourcesFormDto
import com.samaxxw.dto.model.ProductCategoryDto
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dto.model.ProductResourceDto
import com.samaxxw.dto.paged.PagedDto
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.ProductCategoryDtoService
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.dtoservice.SalesTrxDtoService
import com.samaxxw.exception.ValidationException
import com.samaxxw.mapper.ProductMapper
import com.samaxxw.mapper.ProductResourcesMapper
import com.samaxxw.model.Product
import com.samaxxw.model.ProductResource
import com.samaxxw.service.ProductResourcesService
import com.samaxxw.service.ProductService
import com.samaxxw.service.SalesTrxService
import com.samaxxw.service.ui.BreadCrumbService
import com.samaxxw.util.PaginationUtils
import com.samaxxw.util.ProductStatusEnum
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
@RequestMapping(value = [ROOT])
class ProductController : AdvancedBaseRestController<ProductDto, CreateProductDto, UpdateProductDto>() {

    @Autowired
    var productDtoService : ProductDtoService? = null

    @Autowired
    var productService : ProductService? = null

    @Autowired
    var productCategoryDtoService : ProductCategoryDtoService? = null

    @Autowired
    var productResourcesService : ProductResourcesService? = null

    @Autowired
    var salesTrxDtoService : SalesTrxDtoService? = null

    @Autowired
    var breadCrumbService : BreadCrumbService? = null

    override val dtoService: BaseDtoService<ProductDto>
        get() = productDtoService!!

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/product"
        const val BINDRESULT_PRODUCTEDITFORM_KEY = "org.springframework.validation.BindingResult.productEditForm"
        const val BINDRESULT_PRODUCTFORM_KEY = "org.springframework.validation.BindingResult.productForm"

    }

    @ModelAttribute("productForm")
    fun productFormDto(): ProductFormDto {
        return ProductFormDto()
    }

    @ModelAttribute("productEditForm")
    fun productEditFormDto(): ProductEditFormDto {
        return ProductEditFormDto()
    }

    @ModelAttribute("productResourcesForm")
    fun productResourcesFormDto(): ProductResourcesFormDto {
        return ProductResourcesFormDto()
    }

    fun addViewModelAttribute(model: Model) {

        model.addAttribute(PAGE_HEADER, "Add Product")
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductAddBreadCrumb())

    }

    fun editViewModelAttribute(model: Model, id: Long) {

        val productDto = productDtoService!!.findById(id)
        model.addAttribute(PAGE_HEADER, "Edit Product")
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductEditBreadCrumb())
        model.addAttribute("productCategoryMap", productCategoryHandler(productDto))


    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_PRODUCT)
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
        model.addAttribute("listUrl", "$ROOT/list")
//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Product Category List
        val productCategoryList : MutableList<ProductCategoryDto> = productCategoryDtoService!!.findAll()
        model.addAttribute("productCategoryList", productCategoryList)

        model.addAttribute(NAV_KEY, NavKeyConstant.NAV_PRODUCT)


    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            productSearchModel: ProductSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SEARCH_FILTER_NAME_PRODUCT, productSearchModel)
        }

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        model.addAttribute(PAGE_HEADER, "Product Management")
        model.addAttribute("status", ProductStatusEnum.getDisplayMap())
        model.addAttribute("list", result)

        // return for pagination indicator
        model.addAttribute("pageNum", page)
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductRootBreadCrumb(true))

        return getViewPath("product/list", request)

    }

    @GetMapping(value = ["/add"])
    fun add(model: Model): String {
        addViewModelAttribute(model)
        return getViewPath("product/add", request!!)
    }

    @GetMapping(value = ["/edit/{id}"])
    fun edit(model: Model, @PathVariable id: Long): String {

        val productDto = productDtoService!!.findById(id)
        model.addAttribute("productEditForm", convertEditFormDto(productDto))
        editViewModelAttribute(model, id)

        return getViewPath("product/edit", request!!)
    }

    @GetMapping(value = ["/edit/resources/{id}"])
    fun resourcesList(model: Model, @PathVariable id: Long): String {
        model.addAttribute(PAGE_HEADER, "Product Resources Configuration")
        val product = ProductMapper.INSTANCE.toEntity(productDtoService?.findById(id)!!)

        val resourcesList : MutableList<ProductResourceDto> = mutableListOf()
        productResourcesService?.findByProduct(product)!!.forEach {
            resourcesList.add(ProductResourcesMapper.INSTANCE.toDTO(it))
        }

        model.addAttribute("product", product)
        model.addAttribute("resourceList", resourcesList)
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductResourcesEditBreadCrumb(id))


        return getViewPath("product/resource/list", request!!)
    }

    // User purchase list
    @RequestMapping(value = ["/{id}/user/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @PathVariable("id") productId: Long,
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            productSalesTrxSearchModel: ProductSalesTrxSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        val paginationDto = PaginationUtils.createPaginationDto(page, per_page, sortField, direction)

        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SearchModelName.SEARCH_FILTER_NAME_PRODUCT_SALES_TRX, productSalesTrxSearchModel)
        } else if (request.method == RequestMethod.GET.name) {
            val productSalesTrxFilter = ProductSalesTrxSearchModel()
            productSalesTrxFilter.productId = productId // default filter by user ID
            request.session.setAttribute(SearchModelName.SEARCH_FILTER_NAME_PRODUCT_SALES_TRX, productSalesTrxFilter)
        }

        val result = salesTrxDtoService?.getProductSalesTrxList(productId, paginationDto)
        model.addAttribute(PAGE_HEADER, "Product User Sales Record")
        model.addAttribute("productUserSalesUrl", "$ROOT/$productId/user/list")
        model.addAttribute("productId", productId)
        model.addAttribute("list", result)
        model.addAttribute("searchProductSalesTrxFilter", SEARCH_FILTER_NAME_PRODUCT_SALES_TRX)

        // return for pagination indicator
        model.addAttribute("pageNum", page)
        model.addAttribute(BREABCRUMB, breadCrumbService?.getProductSalesRootBreadCrumb(productId, true))


        return getViewPath("product/user/list", request)
    }

    @PostMapping(value = ["/edit"])
    fun edit(@ModelAttribute("productEditForm") @Valid productEditForm: ProductEditFormDto,
            result: BindingResult,
            attr : RedirectAttributes,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        // ## Prevent UI components missing ##
        editViewModelAttribute(model, productEditForm.id!!)

        try {
            // Do a form validation check first
            if (result.hasErrors()) {
                return getViewPath("product/edit", request)
            }
            var product = productService!!.update(productEditForm)
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Product Updated"))
            // Update category map
            model.addAttribute("productCategoryMap", productCategoryHandler(ProductMapper.INSTANCE.toDTO(product)))

        } catch (ve: ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
        }

        return getViewPath("product/edit", request)
    }

    @PostMapping(value = ["/add"])
    fun add(@ModelAttribute("productForm") @Valid productDto: ProductFormDto,
            result: BindingResult,
            model: Model,
            attr : RedirectAttributes,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        var createdProduct : Product

        model.addAttribute(addViewModelAttribute(model))

        try {
            // Do a form validation check first
            if (result.hasErrors()) {
                return getViewPath("product/add", request)
            }
            createdProduct = productService!!.save(productDto)
        } catch (ve : ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
            return getViewPath("product/add", request)
        }
        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Product Created"))
        model.addAttribute("productForm", ProductFormDto())
        return getViewPath("product/add", request)
    }

    override fun searchWithPaging(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            request: HttpServletRequest, response: HttpServletResponse): PagedDto<ProductDto> {
        return super.searchWithPaging(page, per_page, sortField,  direction, request, response)
    }

    fun productCategoryHandler(productDto: ProductDto) : LinkedHashMap<String, Boolean> {

        val productCategoryMap : LinkedHashMap<String, Boolean> = LinkedHashMap()
        val categoryList = productCategoryDtoService!!.findAll()
        categoryList.forEach { category ->

            var selected = false
            productDto.productCategories!!.forEach{
                productCategory ->
                if (category.name == productCategory.name) {
                    selected = true
                }
            }

            productCategoryMap[category.name!!] = selected

        }

        return productCategoryMap

    }

    fun convertEditFormDto(productDto: ProductDto) : ProductEditFormDto {
        // TODO move to mapper
        val result = ProductEditFormDto()
        result.id = productDto.id
        result.description = productDto.description
        result.productCategories = productDto.productCategories!!.map {  categoryDto -> categoryDto.name  }.toMutableList()
        result.price = productDto.price
        result.discount = productDto.discount
        result.name = productDto.name
        result.status = productDto.status
        result.abbreviation = productDto.abbreviation

        return result
    }
}