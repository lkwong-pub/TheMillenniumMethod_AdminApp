package com.samaxxw.controller

import com.samaxxw.controller.AdvancedBaseRestController.Companion.BIND_RESULT_KEY
import com.samaxxw.controller.AdvancedBaseRestController.Companion.BREABCRUMB
import com.samaxxw.controller.AdvancedBaseRestController.Companion.PAGE_HEADER
import com.samaxxw.controller.AdvancedBaseRestController.Companion.SYSTEM_MSG_KEY
import com.samaxxw.controller.PageResourcesController.Companion.ROOT
import com.samaxxw.controller.searchModel.PageResourcesSearchModel
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_PAGE_RESOURCES
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.form.pageResources.*
import com.samaxxw.dtoservice.PageResourcesDtoService
import com.samaxxw.exception.SystemErrorException
import com.samaxxw.exception.ValidationException
import com.samaxxw.mongo.service.PageResourcesService
import com.samaxxw.service.ui.BreadCrumbService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
@RequestMapping(value = [ROOT])
class PageResourcesController : AbstractRestHandler() {

    @Autowired
    val pageResourcesService: PageResourcesService? = null

    @Autowired
    var pageResourcesDtoService : PageResourcesDtoService? = null

    @Autowired
    var breadCrumbService : BreadCrumbService? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/page-resources"
        const val BINDRESULT_PAGERESOURCESFORM_KEY = "org.springframework.validation.BindingResult.pageResourcesForm"
        const val BINDRESULT_PAGERESOURCESEDITFORM_KEY = "org.springframework.validation.BindingResult.pageResourcesEditForm"
        const val BINDRESULT_PAGERESOURCESBATCHFORM_KEY = "org.springframework.validation.BindingResult.pageResourcesBatchForm"

        const val PAGERESOURCESBATCHFORM_SUBMIT_KEY = "pageResourcesBatchFormSubmit"

    }

    @ModelAttribute("pageResourcesForm")
    fun pageResourcesForm(): PageResourcesForm {
        return PageResourcesForm()
    }

    @ModelAttribute("pageResourcesEditForm")
    fun pageResourcesEditForm(): PageResourcesEditForm {
        return PageResourcesEditForm()
    }

    @ModelAttribute("pageResourcesBatchForm")
    fun pageResourcesBatchForm(): PageResourcesBatchForm {
        return PageResourcesBatchForm()
    }

    fun addViewModelAttribute(model: Model) {
        model.addAttribute(PAGE_HEADER, "Add Page Resources")
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesAddBreadCrumb())

    }

    fun addBatchViewModelAttribute(model: Model) {
        model.addAttribute(PAGE_HEADER, "Add Page Resources (Batch)")
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesAddBreadCrumb())
    }

    fun editViewModelAttribute(model: Model, id: String) {
        model.addAttribute(PAGE_HEADER, "Modify Page Resources")
        model.addAttribute("pageResourcesEditForm", pageResourcesDtoService?.getPageResourcesEditForm(id))
        model.addAttribute(BREABCRUMB, breadCrumbService?.getPageResourcesEditBreadCrumb())
    }

    fun editViewModelAttribute(model: Model, section: String, language: String, name: String) {
        model.addAttribute(PAGE_HEADER, "Modify Page Resources")
        model.addAttribute("pageResourcesEditForm", pageResourcesDtoService?.getPageResourcesEditForm(section, language, name))
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesEditFromSectionItemListBreadCrumb(PageResourcesSectionEnum.findByPersistName(section).get()))
    }

    fun listViewModelAttribute(model: Model, page: Int?, per_page: Int?, sortField: String, direction: String) {

        val result = pageResourcesDtoService?.searchWithPaging(page, per_page, sortField, direction, request!!)
        model.addAttribute(PAGE_HEADER, "Page Resources")
        model.addAttribute("list", result)

        // return for pagination indicator
        model.addAttribute("pageNum", page)
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesRootBreadCrumb(true))
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_PAGE_RESOURCES)
//        model.addAttribute("url", baseUrl + "/" + ROOT_URL + "/")
//        model.addAttribute("listUrl", baseUrl + "/" + ROOT_URL + "/")
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
        model.addAttribute("sectionListUrl", "$ROOT/section/list")
        model.addAttribute("sectionItemListUrl", "$ROOT/section")
        model.addAttribute("deleteUrl", "$ROOT/delete")
        // Selection
        model.addAttribute("typeSelection", PageResourcesTypeEnum.getDisplayMap())
        model.addAttribute("sectionSelection", PageResourcesSectionEnum.getDisplayMap())
        model.addAttribute("languageSelection", PageResourcesLanguageEnum.getDisplayMap())

        model.addAttribute(AdvancedBaseRestController.NAV_KEY, NavKeyConstant.NAV_PAGE_RESOURCES)

//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")

    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            pageResourcesSearchModel: PageResourcesSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {


        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SEARCH_FILTER_NAME_PAGE_RESOURCES, pageResourcesSearchModel)
        }

//        val result = pageResourcesDtoService?.searchWithPaging(page, per_page, sortField, direction, request)
//        model.addAttribute(PAGE_HEADER, "Page Resources")
//        model.addAttribute("list", result)
//
//        // return for pagination indicator
//        model.addAttribute("pageNum", page)
//        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesRootBreadCrumb(true))

        listViewModelAttribute(model, page, per_page, sortField, direction)
        return getViewPath("page-resources/list", request)
    }

    // TODO section list
    @RequestMapping(value = ["/section/list"], method = [RequestMethod.GET])
    fun list(
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        val result = PageResourcesSectionEnum.getDisplayMap()
        model.addAttribute(PAGE_HEADER, "Page Resources - Section List")
        model.addAttribute("sectionMap", result)
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesSectionListBreadCrumb(false))

        return getViewPath("page-resources/sectionList", request)
    }

    fun sectionListItemModelAttribute(model: Model, section: PageResourcesSectionEnum) {

        val result = pageResourcesDtoService?.getPageResourcesSectionItem(section.persistName)
        model.addAttribute(PAGE_HEADER, "Page Resources - Section $section")
        model.addAttribute("addUrl", "$ROOT/add/$section")
        model.addAttribute("list", result)
        model.addAttribute("languageSelection", PageResourcesLanguageEnum.getDisplayMap())
        model.addAttribute("section", section)
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesSectionItemListBreadCrumb(section.displayName))

    }

    // TODO page resources list by section with language indicator
    // Get all data by section
    // group by language and group in single record
    @RequestMapping(value = ["/section/{section}/list"], method = [RequestMethod.GET])
    fun list(
            @PathVariable("section") section: PageResourcesSectionEnum,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        sectionListItemModelAttribute(model, section)
        return getViewPath("page-resources/sectionItemList", request)
    }



    @GetMapping(value = ["/add"])
    fun add(model: Model): String {

        addViewModelAttribute(model)
        model.addAttribute(PAGE_HEADER, "Add Page Resources")
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesAddBreadCrumb())

//        if (model.asMap().containsKey(BIND_RESULT_KEY)) {
//            // ## Passed from last request ## //
//            // ## Change key back to result binding key for thymeleaf ##//
//            model.addAttribute(BINDRESULT_PAGERESOURCESFORM_KEY,
//                    model.asMap()[BIND_RESULT_KEY])
//        }

        return getViewPath("page-resources/add" , request!!)
    }

    @GetMapping(value = ["/add/batch"])
    fun addBatch(model: Model): String {

        addBatchViewModelAttribute(model)
        model.addAttribute("pageResourcesBatchForm", PageResourcesBatchForm(mutableListOf(PageResourcesForm(PageResourcesSectionEnum.INDEX, "", ""))))
        return getViewPath("page-resources/addBatch" , request!!)

    }


    @GetMapping(value = ["/add/{id}/{language}"])
    fun add(
            model: Model,
            @PathVariable("language") language: String,
            @PathVariable("id") id: String): String {

        var referredPageResource = pageResourcesService?.getReferencedPageResourcesForm(id, language)
        model.addAttribute(PAGE_HEADER, "Add Page Resources")

        // TODO get related page resources

        model.addAttribute("pageResourcesForm", referredPageResource)
        if (model.asMap().containsKey(BIND_RESULT_KEY)) {
            // ## Passed from last request ## //
            // ## Change key back to result binding key for thymeleaf ##//
            model.addAttribute(BINDRESULT_PAGERESOURCESFORM_KEY,
                    model.asMap()[BIND_RESULT_KEY])
        }

        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesAddFromSectionItemListBreadCrumb(referredPageResource!!.section!!))

        return getViewPath("page-resources/add" , request!!)
    }

    @GetMapping(value = ["/add/{section}/{id}/{language}"])
    fun add(
            model: Model,
            @PathVariable("section") section: String,
            @PathVariable("language") language: String,
            @PathVariable("id") id: String): String {

        var referredPageResource = pageResourcesService?.getReferencedPageResourcesForm(id, language)
        model.addAttribute(PAGE_HEADER, "Add Page Resources")

        // TODO get related page resources

        model.addAttribute("pageResourcesForm", referredPageResource)
        if (model.asMap().containsKey(BIND_RESULT_KEY)) {
            // ## Passed from last request ## //
            // ## Change key back to result binding key for thymeleaf ##//
            model.addAttribute(BINDRESULT_PAGERESOURCESFORM_KEY,
                    model.asMap()[BIND_RESULT_KEY])
        }

        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesAddFromSectionItemListBreadCrumb(referredPageResource!!.section!!))

        return getViewPath("page-resources/add" , request!!)

    }

    @GetMapping(value = ["/add/{section}"])
    fun add(
            model: Model,
            @PathVariable("section") section: String) : String {

        var referredPageResource = pageResourcesService?.getReferencedPageResourcesForm(section)
        model.addAttribute(PAGE_HEADER, "Add Page Resources")

        // TODO get related page resources

        model.addAttribute("pageResourcesForm", referredPageResource)
        if (model.asMap().containsKey(BIND_RESULT_KEY)) {
            // ## Passed from last request ## //
            // ## Change key back to result binding key for thymeleaf ##//
            model.addAttribute(BINDRESULT_PAGERESOURCESFORM_KEY,
                    model.asMap()[BIND_RESULT_KEY])
        }

        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesAddFromSectionItemListBreadCrumb(referredPageResource!!.section!!))

        return getViewPath("page-resources/add" , request!!)

    }

    @PostMapping(value = ["/add"])
    fun add(@ModelAttribute("pageResourcesForm") @Valid pageResourcesFormDto: PageResourcesForm,
            result: BindingResult,
            model: Model,
            attr : RedirectAttributes,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        addViewModelAttribute(model)

        try {
            // Do a form validation check first
            if (result.hasErrors()) {
                return getViewPath("page-resources/add" , request)
            }
            pageResourcesService?.save(pageResourcesFormDto)
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Page Resources Created"))

            var referredPageResource = pageResourcesService?.getReferencedPageResourcesForm(pageResourcesFormDto.section!!.name)
            model.addAttribute("pageResourcesForm", referredPageResource)
            model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesAddFromSectionItemListBreadCrumb(referredPageResource!!.section!!))


        } catch (ve : ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
        }

        return getViewPath("page-resources/add" , request)

    }

    @PostMapping(value = ["/add/batch"])
    fun addBatch(@ModelAttribute("pageResourcesBatchForm") @Valid pageResourcesBatchForm: PageResourcesBatchForm,
            result: BindingResult,
            model: Model,
            attr : RedirectAttributes,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        addBatchViewModelAttribute(model)

        try {
            // Do a form validation check first
//            attr.addFlashAttribute(PAGERESOURCESBATCHFORM_SUBMIT_KEY, PAGERESOURCESBATCHFORM_SUBMIT_KEY).addFlashAttribute(PAGERESOURCESBATCHFORM_SUBMIT_KEY, pageResourcesBatchForm)
            if (result.hasErrors()) {
//                attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
                return getViewPath("page-resources/addBatch" , request)
            }
            pageResourcesService?.saveBatch(pageResourcesBatchForm)
            // Reset for model attribute
            model.addAttribute("pageResourcesBatchForm", PageResourcesBatchForm(mutableListOf(PageResourcesForm(PageResourcesSectionEnum.INDEX, "", ""))))

        } catch (ve : ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
            model.addAttribute("pageResourcesBatchForm", pageResourcesBatchForm.pageResourcesFormList)
//            attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//            return "redirect:/page-resources/add/batch"
        } catch (se : SystemErrorException) {
            model.addAttribute("pageResourcesBatchForm", pageResourcesBatchForm.pageResourcesFormList)
//            attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, se.message))
//            return "redirect:/page-resources/add/batch"
        }

        // Reset for model attribute
        return getViewPath("page-resources/addBatch" , request)

//        attr.addFlashAttribute(PAGERESOURCESBATCHFORM_SUBMIT_KEY, PAGERESOURCESBATCHFORM_SUBMIT_KEY).addFlashAttribute(PAGERESOURCESBATCHFORM_SUBMIT_KEY, PageResourcesBatchForm())
//        attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.INFO, "Page Resources Created"))
//        return "redirect:/page-resources/add/batch"
        // TODO implement system message handling
    }


    @GetMapping(value = ["/edit/{id}"])
    fun edit(model: Model, @PathVariable id: String): String {

        editViewModelAttribute(model, id)

        // TODO rename modelmap key for work around

//        if (model.asMap().containsKey(BIND_RESULT_KEY))
//        {
//            // ## Passed from last request ## //
//            // ## Change key back to result binding key for thymeleaf ##//
//            model.addAttribute(BINDRESULT_PAGERESOURCESEDITFORM_KEY,
//                    model.asMap()[BIND_RESULT_KEY])
//        }

        return getViewPath("page-resources/edit" , request!!)
//        return "page-resources/edit"
    }


    @GetMapping(value = ["/edit/{section}/{language}/{name}"])
    fun edit(model: Model, @PathVariable("section") section: String, @PathVariable("language") language: String, @PathVariable("name") name: String): String {

        editViewModelAttribute(model, section, language, name)
//        model.addAttribute(PAGE_HEADER, "Modify Page Resources")
//        model.addAttribute("pageResourcesEditForm", pageResourcesDtoService?.getPageResourcesEditForm(section, language, name))
//        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPageResourcesEditFromSectionItemListBreadCrumb(PageResourcesSectionEnum.findByPersistName(section).get()))

        // TODO rename modelmap key for work around

//        if (model.asMap().containsKey(BIND_RESULT_KEY))
//        {
//            // ## Passed from last request ## //
//            // ## Change key back to result binding key for thymeleaf ##//
//            model.addAttribute(BINDRESULT_PAGERESOURCESEDITFORM_KEY,
//                    model.asMap()[BIND_RESULT_KEY])
//        }
        return getViewPath("page-resources/edit" , request!!)

//        return "page-resources/edit"
    }

    @PostMapping(value = ["/edit"])
    fun edit(@ModelAttribute("pageResourcesEditForm") @Valid pageResourcesEditForm: PageResourcesEditForm,
             result: BindingResult,
             attr : RedirectAttributes,
             model: Model,
             response: HttpServletResponse,
             request: HttpServletRequest
    ): String {
        try {
            // Do a form validation check first
            if (result.hasErrors()) {
//                // ## Note - pass binding result to next request ## //
//                // ## Original key unable to put as a key ## //
//                attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//                return "redirect:/page-resources/edit/" + pageResourcesEditForm.id
                editViewModelAttribute(model, pageResourcesEditForm.section!!.persistName, pageResourcesEditForm.language!!.persistName, pageResourcesEditForm.name!!)
                return getViewPath("page-resources/edit" , request)

            }
            var pageResources = pageResourcesService!!.update(pageResourcesEditForm)
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Page Resources Updated"))

//            log.debug(pageResources.toString())
        } catch (ve: ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
//            attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//            return "redirect:/page-resources/edit/" + pageResourcesEditForm.id
        }
//        attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.INFO, "Page Resources Updated"))
//        return "redirect:/page-resources/edit/" + pageResourcesEditForm.id
        editViewModelAttribute(model, pageResourcesEditForm.section!!.persistName, pageResourcesEditForm.language!!.persistName, pageResourcesEditForm.name!!)

        return getViewPath("page-resources/edit" , request)


    }

    @GetMapping(value = ["/delete/{id}"])
    fun delete(model: Model, attr : RedirectAttributes,@PathVariable("id") id: String) : String {

        try {
            pageResourcesService?.delete(id)
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Page Resources Deleted"))
            attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Page Resources Deleted"))
        } catch (e: Exception) {
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, e.message))
        }

        listViewModelAttribute(model, DEFAULT_PAGE_NUM.toInt(), DEFAULT_PAGE_SIZE.toInt(), DEFAULT_SORT, DEFAULT_SORT_DIR)

        return getViewPath("page-resources/list" , request!!)
//        return "redirect:/page-resources/list"
    }

    @PostMapping(path = ["/add/item"])
    fun addItem(pageResourcesBatchForm: PageResourcesBatchForm, request: HttpServletRequest, model: Model): String {

        // get last record setting
        val lastRecord = pageResourcesBatchForm.pageResourcesFormList!!.last()

        pageResourcesBatchForm.pageResourcesFormList!!.add(PageResourcesForm(lastRecord.section, lastRecord.type, lastRecord.language))
        model.addAttribute(PAGE_HEADER, "Add Page Resources (Batch)")
//        return "redirect:/page-resources/add/batch"
//
        return if (AJAX_HEADER_VALUE == request.getHeader(AJAX_HEADER_NAME)) {
            // It is an Ajax request, render only #items fragment of the page.
            "page-resources/addBatch :: items"
        } else {
            // It is a standard HTTP request, render whole page.
            "page-resources/addBatch"
        }
    }

    @PostMapping(path = ["/remove/item/{id}"])
    fun removeItem(@PathVariable("id") id: Int, pageResourcesBatchForm: PageResourcesBatchForm, request: HttpServletRequest, model: Model): String {

        // get last record setting
        if (pageResourcesBatchForm.pageResourcesFormList!!.size > 1) {
            pageResourcesBatchForm.pageResourcesFormList!!.removeAt(id)
        }

        model.addAttribute(PAGE_HEADER, "Add Page Resources (Batch)")

        return if (AJAX_HEADER_VALUE == request.getHeader(AJAX_HEADER_NAME)) {
            // It is an Ajax request, render only #items fragment of the page.
            "page-resources/addBatch :: items"
        } else {
            // It is a standard HTTP request, render whole page.
            "page-resources/addBatch"
        }
    }

    @PostMapping(value = ["/delete/batch"], consumes = ["application/json"])
    fun deleteBatch(model: Model,  @RequestBody idList: MutableList<String>) : ResponseEntity<MessageBox> {

        try {
            if (idList.size > 0) {
                pageResourcesService?.delete(idList)
                return ResponseEntity(MessageBox(MessageBoxTypeEnum.SUCCESS, "Page Resources Deleted"), HttpStatus.OK)
            } else {
                return ResponseEntity(MessageBox(MessageBoxTypeEnum.DANGER, "Nothing selected"), HttpStatus.BAD_REQUEST)
            }

//            attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Page Resources Deleted"))
        } catch (e: Exception) {
            return ResponseEntity(MessageBox(MessageBoxTypeEnum.DANGER, e.message), HttpStatus.BAD_REQUEST)

//            attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, e.message))
        }
//        return "redirect:/page-resources/list"
    }
}
