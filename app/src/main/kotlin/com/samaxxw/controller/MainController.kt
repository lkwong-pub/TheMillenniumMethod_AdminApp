package com.samaxxw.controller

import com.samaxxw.controller.AdvancedBaseRestController.Companion.NAV_KEY
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.form.pageResources.PageResourcesLanguageEnum
import com.samaxxw.mongo.service.PageResourcesService
import com.samaxxw.service.UserService
import com.samaxxw.service.report.TestReportService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class MainController : AbstractRestHandler() {

    @Autowired
    val pageResourcesService : PageResourcesService? = null

    @Autowired
    val userService : UserService? = null

    @Autowired
    val testReportService: TestReportService? = null

    @GetMapping("/")
    fun root(model: Model, request: HttpServletRequest): String {
        model.addAttribute("userCount", userService?.getUserCount())
        // Translation process indicator
        // Use English as base number
        model.addAttribute("translationProcess", pageResourcesService?.getTranslationProcessIndicator())
        model.addAttribute(NAV_KEY, NavKeyConstant.NAV_HOME)
//        return "index"
        return if (AJAX_HEADER_VALUE == request.getHeader(AJAX_HEADER_NAME)) {
            // It is an Ajax request, render only #items fragment of the page.
           "index :: content-fragment"
        } else {
            // It is a standard HTTP request, render whole page.
            "index"
        }
    }

    @GetMapping("/login")
    fun login(model: Model, response: HttpServletResponse): String {
        response.addHeader("PageType", "LOGIN")
        response.addHeader("Redirect", "/login")
        return "login"
    }

    @GetMapping("/user")
    fun userIndex(): String {
        return "user/index"
    }

    @GetMapping("/accessDenied")
    fun accessDenied(response: HttpServletResponse): String {
        response.addHeader("PageType", "ACCESS_DENIED")
        response.addHeader("Redirect", "/accessDenied")
        return "401"
    }

    @GetMapping("/message")
    fun message(model: Model): String {
        return "base/message"
    }

    @GetMapping("/adminbsb")
    fun adminBsbTest(mdoel: Model) : String {
        return "adminbsb/test"
    }

    @GetMapping("/mongo_test")
    fun test(model: Model): String {
        model.addAttribute("list", pageResourcesService?.getData())
        return "base/mongo_test"
    }

    @ResponseBody
    @RequestMapping("/test-report")
    fun testReport() : String {
//        print(principal.name)
        return testReportService!!.generateReport()
    }
    
}
