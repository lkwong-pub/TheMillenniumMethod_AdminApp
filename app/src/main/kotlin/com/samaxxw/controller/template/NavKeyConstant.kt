package com.samaxxw.controller.template


class NavKeyConstant {

    companion object {

        val NAV_HOME = "navHome"
        val NAV_USER = "navUser"
        val NAV_PRODUCT = "navProduct"
        val NAV_PAGE_RESOURCES = "navPageResources"
        val NAV_ADMIN = "navAdmin"
    }
}