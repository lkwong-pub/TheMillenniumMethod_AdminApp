package com.samaxxw.controller

import com.samaxxw.controller.UserPurchaseController.Companion.ROOT
import com.samaxxw.controller.searchModel.SalesSearchModel
import com.samaxxw.controller.searchModel.SalesTrxSearchModel
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_SALES
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_SALES_TRX
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.create.CreateSalesDto
import com.samaxxw.dto.create.CreateSalesTrxDto
import com.samaxxw.dto.create.UpdateSalesDto
import com.samaxxw.dto.create.UpdateSalesTrxDto
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dto.model.SalesDto
import com.samaxxw.dto.model.SalesTrxDto
import com.samaxxw.dto.model.UserDto
import com.samaxxw.dtoservice.*
import com.samaxxw.service.ui.BreadCrumbService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Controller
@RequestMapping(value = [ROOT])
class UserPurchaseController : AdvancedBaseRestController<SalesTrxDto, CreateSalesTrxDto, UpdateSalesTrxDto>() {

    @Autowired
    var salesTrxDtoService : SalesTrxDtoService? = null

    @Autowired
    val productDtoService : ProductDtoService? = null

    @Autowired
    var breadCrumbService : BreadCrumbService? = null

    @Autowired
    var userDtoService : UserDtoService? = null

    override val dtoService: BaseDtoService<SalesTrxDto>
        get() = salesTrxDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/purchase"
//        const val BINDRESULT_USEREDITFORM_KEY = "org.springframework.validation.BindingResult.userEditForm"
        
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_SALES_TRX)
//        model.addAttribute("url", baseUrl + "/" + ROOT_URL + "/")
//        model.addAttribute("listUrl", baseUrl + "/" + ROOT_URL + "/")
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Role List
        val productList : MutableList<ProductDto> = productDtoService!!.findAll()
//        model.addAttribute("productList", productList)
        model.addAttribute(NAV_KEY, NavKeyConstant.NAV_USER)

    }

    @RequestMapping(value = ["/user/{userId}/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @PathVariable("userId") userId: String,
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            salesTrxSearchModel: SalesTrxSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, salesTrxSearchModel)
        } else if (request.method == RequestMethod.GET.name) {
            val userSalesTrxFilter = SalesTrxSearchModel()
            userSalesTrxFilter.userId = userId.toLong() // default filter by user ID
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, userSalesTrxFilter)
        }

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        val user = getUser(userId.toLong())
        model.addAttribute(PAGE_HEADER, "User Purchase Record - $userId (${user.firstName} ${user.lastName})")
        model.addAttribute("list", result)
        model.addAttribute("userId", userId)

        // return for pagination indicator
        model.addAttribute("pageNum", page)
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getUserPurchaseRecordRootBreadCrumb(null, true))

        return getViewPath("user/purchase/list", request)
        
    }

    fun getUser(userId: Long) : UserDto {
        return userDtoService?.findById(userId)!!
    }



}