package com.samaxxw.controller

import com.samaxxw.controller.UserController.Companion.ROOT
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_USER
import com.samaxxw.controller.searchModel.UserSearchModel
import com.samaxxw.controller.template.BreadcrumbListDto
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.create.CreateUserDto
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.form.user.UserEditFormDto
import com.samaxxw.dto.form.user.UserFormDto
import com.samaxxw.dto.model.RoleDto
import com.samaxxw.dto.model.UserDto
import com.samaxxw.dto.paged.PagedDto
import com.samaxxw.dto.update.UpdateUserDto
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.RoleDtoService
import com.samaxxw.dtoservice.UserDtoService
import com.samaxxw.exception.ValidationException
import com.samaxxw.service.UserService
import com.samaxxw.service.ui.BreadCrumbService
import com.samaxxw.util.UserStatusEnum
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
@RequestMapping(value = [ROOT])
class UserController : AdvancedBaseRestController<UserDto, CreateUserDto, UpdateUserDto>() {

    @Autowired
    var userDtoService : UserDtoService? = null

    @Autowired
    var userService : UserService? = null

    @Autowired
    var roleDtoService : RoleDtoService? = null

    @Autowired
    var breadCrumbService : BreadCrumbService? = null


    override val dtoService: BaseDtoService<UserDto>
        get() = userDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/user"
        const val BINDRESULT_USEREDITFORM_KEY = "org.springframework.validation.BindingResult.userEditForm"
        const val BINDRESULT_USERFORM_KEY = "org.springframework.validation.BindingResult.userForm"

    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_USER)
//        model.addAttribute("url", baseUrl + "/" + ROOT_URL + "/")
//        model.addAttribute("listUrl", baseUrl + "/" + ROOT_URL + "/")
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
        model.addAttribute("listUrl", "$ROOT/list")
        model.addAttribute("userTrxUrl", "/trx/user")
        model.addAttribute("userPurchaseUrl", "/purchase/user")
        model.addAttribute("userGrantUrl", "/grant/user")
        model.addAttribute("userStatusEnum", UserStatusEnum.getDisplayMap())

//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Role List
        val roleList : MutableList<RoleDto> = roleDtoService!!.findAll()
        model.addAttribute("roleList", roleList)

        model.addAttribute(NAV_KEY, NavKeyConstant.NAV_USER)

    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            userSearchModel: UserSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SEARCH_FILTER_NAME_USER, userSearchModel)
        }

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        model.addAttribute(PAGE_HEADER, "User Management")
        model.addAttribute("list", result)
        model.addAttribute(BREABCRUMB, breadCrumbService?.getUserRootBreadCrumb(true))

        // return for pagination indicator
        model.addAttribute("pageNum", page)

//        return "user/list"

        return getViewPath("user/list", request)
    }


    @ModelAttribute("userForm")
    fun userFormDto(): UserFormDto {
        return UserFormDto()
    }

    @ModelAttribute("userEditForm")
    fun userEditFormDto(): UserEditFormDto {
        return UserEditFormDto()
    }


    fun addViewModelAttribute(model: Model) {
        model.addAttribute(PAGE_HEADER, "Add User")
        model.addAttribute(BREABCRUMB, breadCrumbService?.getUserAddBreadCrumb())
    }


    fun editViewModelAttribute(model: Model, id: Long) {

        val userDto = userDtoService!!.findById(id)
        model.addAttribute(PAGE_HEADER, "Modify User")
        model.addAttribute(BREABCRUMB, breadCrumbService?.getUserEditBreadCrumb())
        model.addAttribute("userRoleMap", userRoleHandler(userDto))

    }

    @GetMapping(value = ["/add"])
    fun add(model: Model): String {

        model.addAttribute(addViewModelAttribute(model))
//        model.addAttribute(PAGE_HEADER, "Add User")
//        model.addAttribute(BREABCRUMB, breadCrumbService?.getUserAddBreadCrumb())

//        if (model.asMap().containsKey(BIND_RESULT_KEY))
//        {
//            // ## Passed from last request ## //
//            // ## Change key back to result binding key for thymeleaf ##//
//            model.addAttribute(UserController.BINDRESULT_USERFORM_KEY,
//                    model.asMap()[BIND_RESULT_KEY])
//        }
        return getViewPath("user/add", request!!)
//        return "user/add :: content-fragment"
    }

    @GetMapping(value = ["/edit/{id}"])
    fun edit(model: Model, @PathVariable id: Long): String {
        val userDto = userDtoService!!.findById(id)

//        model.addAttribute(PAGE_HEADER, "Modify User")
//        model.addAttribute(BREABCRUMB, breadCrumbService?.getUserEditBreadCrumb())
        model.addAttribute("userEditForm", convertEditFormDto(userDto))
//        model.addAttribute("userRoleMap", userRoleHandler(userDto))
        editViewModelAttribute(model, id)
        // TODO rename modelmap key for work around

//        if (model.asMap().containsKey(BIND_RESULT_KEY))
//        {
//            // ## Passed from last request ## //
//            // ## Change key back to result binding key for thymeleaf ##//
//            model.addAttribute(BINDRESULT_USEREDITFORM_KEY,
//                    model.asMap()[BIND_RESULT_KEY])
//        }

        return getViewPath("user/edit", request!!)
//        return "user/edit :: content-fragment"
    }

    @PostMapping(value = ["/edit"])
    fun edit(@ModelAttribute("userEditForm") @Valid userEditForm: UserEditFormDto,
            result: BindingResult,
            attr : RedirectAttributes,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {
//        try {
//            // Do a form validation check first
//            if (result.hasErrors()) {
//                // ## Note - pass binding result to next request ## //
//                // ## Original key unable to put as a key ## //
//                attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//
//                return "redirect:/user/edit/" + userEditForm.id
//            }
//            var user = userService!!.update(userEditForm)
//            log.debug(user.toString())
//        } catch (ve: ValidationException) {
//            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
//            attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//            return "redirect:/user/edit/" + userEditForm.id
//        }
//        attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.INFO, "User Updated"))
//        return "redirect:/user/edit/" + userEditForm.id

        editViewModelAttribute(model, userEditForm.id!!)

        try {
            // Do a form validation check first
            if (result.hasErrors()) {
                // ## Note - pass binding result to next request ## //
                // ## Original key unable to put as a key ## //
//                attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)

                return getViewPath("user/edit", request)
            }
            var user = userService!!.update(userEditForm)
            log.debug(user.toString())
        } catch (ve: ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
//            attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//            model.addAttribute(BIND_RESULT_KEY, result)
            return getViewPath("user/edit", request)
        }
//        attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.INFO, "User Updated"))

        // Update View
        val userDto = userDtoService!!.findById(userEditForm.id!!)
        model.addAttribute("userRoleMap", userRoleHandler(userDto))

        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "User Updated"))


        return getViewPath("user/edit", request)
        // TODO implement system message handling
    }

    @PostMapping(value = ["/add"])
    fun add(@ModelAttribute("userForm") @Valid userDto: UserFormDto,
            result: BindingResult,
            model: Model,
            attr : RedirectAttributes,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        model.addAttribute(addViewModelAttribute(model))

        try {
            // Do a form validation check first
            if (result.hasErrors()) {
//                attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//                return "redirect:/user/add"
                return getViewPath("user/add", request)
            }
            userService!!.save(userDto)
        } catch (ve : ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)


//            attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
//            return "redirect:/user/add"
            return getViewPath("user/add", request)

        }
        // TODO test only
        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "User Created"))
        model.addAttribute("userForm", UserFormDto()) // Reset form
//        attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.INFO, "User Created"))
//        return "redirect:/user/add"
        return getViewPath("user/add", request)
        // TODO implement system message handling
    }

    override fun searchWithPaging(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            request: HttpServletRequest, response: HttpServletResponse): PagedDto<UserDto> {
        return super.searchWithPaging(page, per_page, sortField,  direction, request, response)
    }

    fun userRoleHandler(userDto: UserDto) : LinkedHashMap<String, Boolean> {

        val userRoleMap : LinkedHashMap<String, Boolean> = LinkedHashMap()
        val roleList = roleDtoService!!.findAll()
        roleList.forEach { role ->

            var selected = false
            userDto.roles!!.forEach{
                userRole ->
                if (userRole.name == role.name) {
                    selected = true
                }
            }

            userRoleMap[role.name!!] = selected

        }

        return userRoleMap

    }

    fun convertEditFormDto(userDto: UserDto) : UserEditFormDto {
        // TODO move to mapper
        val result = userEditFormDto()
        result.id = userDto.id
        result.roles = userDto.roles!!.map {  roleDto -> roleDto.name  }.toMutableList()
        result.email = userDto.email
        result.firstName = userDto.firstName
        result.lastName = userDto.lastName

        return result
    }
}