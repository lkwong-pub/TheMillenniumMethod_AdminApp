package com.samaxxw.controller

import com.samaxxw.controller.UserGrantProductController.Companion.ROOT
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.form.user.product.GrantProductFormDto
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.dtoservice.UserDtoService
import com.samaxxw.exception.ValidationException
import com.samaxxw.service.GrantProductService
import com.samaxxw.service.ui.BreadCrumbService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
@RequestMapping(value = [ROOT])
class UserGrantProductController : AbstractRestHandler() {

    @Autowired
    var grantProductService : GrantProductService? = null

    @Autowired
    val productDtoService : ProductDtoService? = null

    @Autowired
    val userDtoService : UserDtoService? = null

    @Autowired
    var breadCrumbService : BreadCrumbService? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/grant"
//        const val BINDRESULT_USEREDITFORM_KEY = "org.springframework.validation.BindingResult.userEditForm"

    }

    @ModelAttribute("grantProductForm")
    fun grantProductFormDto(): GrantProductFormDto {
        return GrantProductFormDto()
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {
        model.addAttribute(AdvancedBaseRestController.PAGE_HEADER, "Grant Product to User")

        model.addAttribute("submitUrl", "$ROOT/submit")
        // Role List
        val productList : MutableList<ProductDto> = productDtoService!!.findAll()
        model.addAttribute("productList", productList)

        model.addAttribute(AdvancedBaseRestController.NAV_KEY, NavKeyConstant.NAV_USER)

        // Breadcrumb
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getUserGrantProductBreadCrumb())

    }

    @RequestMapping(value = ["/user/{userId}"], method = [RequestMethod.GET])
    fun grantProductPage(
            @PathVariable("userId") userId: String,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {
        val grantProductFormDto = GrantProductFormDto()
        grantProductFormDto.userId = userId.toLong()
        model.addAttribute("user", userDtoService?.findById(userId.toLong()))
        model.addAttribute("grantProductForm", grantProductFormDto)
//        model.addAttribute("userId", userId)
        return getViewPath("user/product/grant", request)
    }

    @RequestMapping(value = ["/submit"], method = [RequestMethod.POST])
    fun grantProduct(
            @ModelAttribute("grantProductForm") @Valid grantProductForm: GrantProductFormDto,
            result: BindingResult,
            model: Model,
            attr : RedirectAttributes,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {
//        try {
//            // Do a form validation check first
//            model.addAttribute("user", userDtoService?.findById(grantProductForm.userId!!))
//
//            if (result.hasErrors()) {
//                return "user/product/grant"
//            }
//
//            // TODO Grant product service
//            grantProductService?.grantProduct(grantProductForm)
//        } catch (ve : ValidationException) {
//            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
//            return "user/product/grant"
//        } catch (e : Exception) {
//            e.printStackTrace()
//            attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY,
//                    AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "System Error ! Failed to Product Granted to User '${e.message}'"))
//            return "redirect:/grant/user/${grantProductForm.userId}"
//        }
//
//        // TODO Get user and product info
//        attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Product Granted to User "))
//        return "redirect:/grant/user/${grantProductForm.userId}"
        try {
            // Do a form validation check first
            model.addAttribute("user", userDtoService?.findById(grantProductForm.userId!!))

            if (result.hasErrors()) {
                return "user/product/grant :: content-fragment"
            }
            // TODO Grant product service
            grantProductService?.grantProduct(grantProductForm)
        } catch (ve : ValidationException) {
            result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
            return getViewPath("user/product/grant", request)
        } catch (e : Exception) {
            e.printStackTrace()
            model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "System Error ! Failed to Product Granted to User '${e.message}'"))
            return getViewPath("user/product/grant", request)
        }

        // TODO Get user and product info
        model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Product Granted to User "))
        return getViewPath("user/product/grant", request)
    }

}