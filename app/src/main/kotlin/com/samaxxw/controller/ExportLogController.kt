package com.samaxxw.controller

import com.samaxxw.controller.AdvancedBaseRestController.Companion.BREABCRUMB
import com.samaxxw.controller.AdvancedBaseRestController.Companion.NAV_KEY
import com.samaxxw.controller.AdvancedBaseRestController.Companion.PAGE_HEADER
import com.samaxxw.controller.ExportLogController.Companion.ROOT
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_EXPORT_LOG
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.model.ExportLogDto
import com.samaxxw.dto.search.ExportLogSearchModel
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.ExportLogDtoService
import com.samaxxw.dtoservice.ExportLogTypeDtoService
import com.samaxxw.exception.ResourceNotFoundException
import com.samaxxw.mapper.ExportLogCustomMapper
import com.samaxxw.service.impl.ExportLogService
import com.samaxxw.service.impl.PageResourcesExportService
import com.samaxxw.service.ui.BreadCrumbService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.thymeleaf.util.StringUtils
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Controller
@RequestMapping(value = [ROOT])
class ExportLogController : BaseRestController<ExportLogDto>() {

    @Autowired
    var exportLogDtoService: ExportLogDtoService? = null

    @Autowired
    var exportLogService: ExportLogService? = null

    @Autowired
    var breadCrumbService: BreadCrumbService? = null

    @Autowired
    var exportLogTypeDtoService: ExportLogTypeDtoService? = null

    @Autowired
    val pageResourcesExportService = PageResourcesExportService()


    override val dtoService: BaseDtoService<ExportLogDto>
        get() = exportLogDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/admin/export-log"
    }

    fun exportListModelAttribute(model: Model) {
        model.addAttribute(PAGE_HEADER, "Export Log Management")
        model.addAttribute(BREABCRUMB, breadCrumbService?.getUserRootBreadCrumb(true)) // FIXME: export log specific breadcrumb
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_EXPORT_LOG)
        model.addAttribute("baseUrl", ROOT)
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
        model.addAttribute("listUrl", "$ROOT/list")
        model.addAttribute("exportUrl", "${ROOT}/page-resources/export")

        model.addAttribute("exportType", exportLogTypeDtoService?.findAll())
        model.addAttribute(NAV_KEY, NavKeyConstant.NAV_ADMIN)

    }

    @RequestMapping(value = ["/list", "/reload"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            exportLogSearchModel: ExportLogSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SEARCH_FILTER_EXPORT_LOG, exportLogSearchModel)
        }

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        result.content = ExportLogCustomMapper.INSTANCE.dtoListToDTOList(result.content!!)
        model.addAttribute("list", result)
        // return for pagination indicator
        model.addAttribute("pageNum", page)

        exportListModelAttribute(model)

        return getViewPath("admin/export/list", request)
    }

    // TODO reuse this method if there is another type of report have to be exported
    @GetMapping("/page-resources/export")
    fun exportPageResources(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            exportLogSearchModel: ExportLogSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest): String {

        pageResourcesExportService.exportPageResources()
        request.session.setAttribute(SEARCH_FILTER_EXPORT_LOG, exportLogSearchModel)

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        result.content = ExportLogCustomMapper.INSTANCE.dtoListToDTOList(result.content!!)
        model.addAttribute("list", result)
        model.addAttribute("pageNum", page)

        exportListModelAttribute(model)

        model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.INFO, "Export task begin"))
        return getViewPath("admin/export/list", "list-fragment", request)

//        return getViewPath("admin/export/list_fragment", "list-fragment", request)

    }

    @GetMapping(value = ["/{id}/attachment"])
    fun getAttachment(response: HttpServletResponse,
                      @PathVariable id: Long,
                      attr: RedirectAttributes): Any {

        val exportLog = exportLogDtoService?.findById(id)
        val uploadId = exportLog!!.fileAttachment!!.uploadId!!
        val fileMeta = exportLogService?.getFileInfo(uploadId)
        val fileByte = exportLogService?.getExportAttachment(uploadId)
        val resource = ByteArrayResource(fileByte!!)

        if (fileByte.isNotEmpty()) {

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment;filename=" + StringUtils.trim(fileMeta!!.fileName!!))
                    .contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(fileMeta.fileSize!!)
                    .body(resource)
        } else {
            attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.WARNING, "Attachment not found"))
            // redirect to product resources form (through Controller)
            return "redirect:/message"
        }

    }

}