package com.samaxxw.service.impl

import com.samaxxw.dao.ExportLogDao
import com.samaxxw.dao.ExportLogTypeDao
import com.samaxxw.dto.io.FileDownloadMetaResponseDto
import com.samaxxw.model.ExportLog
import com.samaxxw.model.ExportLogType
import com.samaxxw.model.FileAttachment
import com.samaxxw.service.FileDownloadService
import com.samaxxw.service.GoogleFileDownloadService
import com.samaxxw.util.ExportStatus
import com.samaxxw.util.ExportTypeEnum
import org.apache.commons.collections.CollectionUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.TemporalAccessor

@Service
class ExportLogService {

    @Autowired
    val exportLogDao: ExportLogDao? = null

    @Autowired
    val exportLogTypeDao: ExportLogTypeDao? = null

    @Autowired
    val fileDownloadService: GoogleFileDownloadService? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
    }

    @Transactional
    fun appendExportLog(exportLogTypeEnum: ExportTypeEnum) : ExportLog {
        var exportLog = ExportLog()
        exportLog.exportLogType = getExportLogType(exportLogTypeEnum)
        exportLog.status = ExportStatus.IN_PROGRESS.status
        return exportLogDao?.save(exportLog)!!
    }

    @Transactional
    fun updateExportLog(id: Long, duration: Long?, exportStatus: ExportStatus, fileAttachment: FileAttachment) {

        var exportLog = exportLogDao?.findOne(id)!!

        exportLog.status = exportStatus.status

        if (exportStatus == ExportStatus.COMPLETED) {
            // save file attachement
            exportLog.fileAttachment = fileAttachment
            exportLog.exportDuration = duration
        }

        exportLogDao?.save(exportLog)

    }

    fun getExportAttachment(objectId: String): ByteArray {
        return fileDownloadService?.getBytesFile(objectId)!!
    }

    fun getFileInfo(objectId: String): FileDownloadMetaResponseDto {
        return fileDownloadService?.getFileMeta(objectId)!!
    }

    fun getExportLogType(exportLogTypeEnum: ExportTypeEnum): ExportLogType {
        return exportLogTypeDao?.findByExportLogType(exportLogTypeEnum.name)!!
    }


    @Transactional
    fun cleanTimeoutTask() {
        val list = exportLogDao?.findByStatus(ExportStatus.IN_PROGRESS.status)

        if (CollectionUtils.isNotEmpty(list)) {
            // Check time out (expected within 1 hour)
            list!!.forEach {

                val targetTime = LocalDateTime.from(it.createDatetime!!.toInstant().atZone(ZoneId.systemDefault()))
                if (LocalDateTime.now().isAfter(targetTime.plusHours(1))) {
                    it.status = ExportStatus.TERMINATED.status
                    log.error("Export task ID - ${it.id} terminated. Reason: time out")
                }

            }

        }

    }
}