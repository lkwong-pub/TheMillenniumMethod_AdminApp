package com.samaxxw.service.impl

import com.google.gson.GsonBuilder
import com.samaxxw.dto.form.pageResources.PageResourcesSectionEnum
import com.samaxxw.model.FileAttachment
import com.samaxxw.mongo.dao.PageResourcesDao
import com.samaxxw.util.ExportStatus
import com.samaxxw.util.ExportTypeEnum
import org.apache.http.entity.ContentType
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor


@Service
class PageResourcesExportService {

    @Autowired
    val pageResourcedDao: PageResourcesDao? = null

    @Autowired
    val exportLogService: ExportLogService? = null

    @Autowired
    val exportLogFileAttachmentService: ExportLogFileAttachmentService? = null

    private val threadPool = Executors.newCachedThreadPool()

    constructor() {
        (threadPool as ThreadPoolExecutor).maximumPoolSize = 10
    }

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        val exportFileNamePrefix = "export-page-resources-"
        val contentType = ContentType.APPLICATION_JSON
        val exportPurpose = "Exported Page Resources"
    }



    fun exportPageResources() {

        val exportLog = exportLogService?.appendExportLog(ExportTypeEnum.PAGE_RESOURCES)

        val thread = Thread(Runnable {

            log.info("exportPageResources: Begin")
//            Thread.sleep(10000) Throttle test
            // Async task start
//            val pageResources = pageResourcedDao?.findAllBySection(PageResourcesSectionEnum.INDEX.persistName)
            val pageResources = pageResourcedDao?.findAll()

            val gsonBuilder = GsonBuilder()
            val exportedJson = gsonBuilder.create().toJson(pageResources)

            val exportedSource = exportedJson.toByteArray()

            val exportFileAttachment = createFileAttachment(exportedSource)
            exportLogService?.updateExportLog(exportLog!!.id!!, null, ExportStatus.COMPLETED, exportFileAttachment)

            log.info("exportPageResources: Completed")
            // Async task end
        })

        threadPool.submit(thread)

//        return exportedSource

    }


    fun createFileAttachment(source: ByteArray): FileAttachment {
        val fileName = exportFileNamePrefix + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
        return exportLogFileAttachmentService?.save(fileName, ExportTypeEnum.PAGE_RESOURCES, source, contentType, exportPurpose)!!
    }


}