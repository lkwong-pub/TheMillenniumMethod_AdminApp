package com.samaxxw.service.impl

import com.querydsl.core.types.Predicate
import com.samaxxw.controller.searchModel.SearchModelName
import com.samaxxw.dao.BaseDao
import com.samaxxw.dao.ExportLogDao
import com.samaxxw.dao.ExportLogTypeDao
import com.samaxxw.dao.SalesTrxLogDao
import com.samaxxw.dto.create.CreateSalesTrxLogDto
import com.samaxxw.dto.create.UpdateSalesTrxLogDto
import com.samaxxw.dto.model.ExportLogDto
import com.samaxxw.dto.model.SalesTrxLogDto
import com.samaxxw.dto.search.ExportLogSearchModel
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.BaseDtoServiceImpl
import com.samaxxw.dtoservice.ExportLogDtoService
import com.samaxxw.dtoservice.SalesTrxLogDtoService
import com.samaxxw.mapper.ExportLogMapper
import com.samaxxw.mapper.SalesTrxLogMapper
import com.samaxxw.mapper.core.DtoMapper
import com.samaxxw.model.ExportLog
import com.samaxxw.model.ExportLogType
import com.samaxxw.model.QExportLog
import com.samaxxw.model.SalesTrxLog
import com.samaxxw.util.ExportTypeEnum
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.time.DateUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest

@Service
class ExportLogDtoServiceImpl : ExportLogDtoService, BaseDtoServiceImpl<ExportLog, ExportLogDto>() {

    @Autowired
    val dao: ExportLogDao? = null

    @Autowired
    val exportLogTypeDao : ExportLogTypeDao? = null

    override fun getDao(): BaseDao<ExportLog, Long> {
        return dao!!
    }

    override fun getDtoMapper(): DtoMapper<ExportLog, ExportLogDto> {
        return ExportLogMapper.INSTANCE
    }

    override fun getPredicate(request: HttpServletRequest): Predicate {

        // Get Parameter directly from session
        var exportLogSearchModel: ExportLogSearchModel?

        var fromDateStr : String? = ""

        var toDateStr : String? = ""

        var type : ExportTypeEnum? = null

        if (request.session.getAttribute(SearchModelName.SEARCH_FILTER_EXPORT_LOG) != null) {
            exportLogSearchModel = request.session.getAttribute(SearchModelName.SEARCH_FILTER_EXPORT_LOG) as ExportLogSearchModel
            fromDateStr = exportLogSearchModel.fromDate
            toDateStr = exportLogSearchModel.toDate
            type = exportLogSearchModel.exportType
        }

        val q = QExportLog.exportLog
        var s = q.isNotNull

        if (StringUtils.isNotBlank(fromDateStr) && StringUtils.isNotBlank(toDateStr)) {
            val fromDate = DateUtils.parseDate(fromDateStr, "yyyy-MM-dd")
            val toDate = DateUtils.parseDate(toDateStr, "yyyy-MM-dd")
            s = s.and(q.createDatetime.goe(fromDate).and(q.createDatetime.loe(toDate)))
        }

        if (type != null) {
            s = s.and(q.exportLogType.eq(getExportLogType(type.name)))
        }


        return s

    }

    fun getExportLogType(name : String) : ExportLogType {
        return exportLogTypeDao?.findByExportLogType(name)!!
    }
}
