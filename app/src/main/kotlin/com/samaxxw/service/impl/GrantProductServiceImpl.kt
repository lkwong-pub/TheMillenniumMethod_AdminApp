package com.samaxxw.service.impl

import com.samaxxw.dao.*
import com.samaxxw.dto.form.user.product.GrantProductFormDto
import com.samaxxw.dto.model.SalesDto
import com.samaxxw.exception.ValidationException
import com.samaxxw.model.*
import com.samaxxw.service.GrantProductService
import com.samaxxw.service.generator.ReferenceCodeGenerator
import com.samaxxw.util.ProductStatusEnum
import com.samaxxw.util.SalesStatus
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.security.core.userdetails.UserDetails
import java.math.BigDecimal


@Service
@Transactional
class GrantProductServiceImpl : GrantProductService {

    @Autowired
    val salesTrxDao: SalesTrxDao? = null

    @Autowired
    val salesDao: SalesDao? = null

    @Autowired
    val salesTrxLogDao: SalesTrxLogDao? = null

    @Autowired
    val userDao: UserDao? = null

    @Autowired
    val productDao: ProductDao? = null

    @Autowired
    val referenceCodeGenerator : ReferenceCodeGenerator? = null

    val log = LoggerFactory.getLogger(this.javaClass)!!


    override fun grantProduct(grantProductFormDto: GrantProductFormDto) {
        val logMsg = "Requested grant [Product ID - '${grantProductFormDto.productId}'] to [User ID - '${grantProductFormDto.userId}]' "
        log.info(logMsg)
        // Check user is owned the requested product
        val user = getUser(grantProductFormDto.userId!!)
        val product = getProduct(grantProductFormDto.productId!!)
        validateArgs(grantProductFormDto, user, product)
        val totalAmount = calcTotalAmount(mutableListOf(product!!))

        // Create Sales
        val sales = persistSales(product!!)

        // Create Sales Transaction
        val salesTrx = persistSalesTrx(user!!, grantProductFormDto, totalAmount, sales)

        // Create Sales Transaction Log
        val salesTrxLog = persistSalesTrxLog(logMsg, salesTrx)

    }

    fun persistSalesTrx(user: User, grantProductFormDto: GrantProductFormDto, totalAmount: BigDecimal, sales: Sales) : SalesTrx {
        var salesTrx = SalesTrx()
        salesTrx.description = ""
        salesTrx.remarks = "Product granted by [User] : ${getUserDetail().username} - [Remarks] : ${grantProductFormDto.remarks}"
        salesTrx.user = user
        salesTrx.trxReferenceCode = referenceCodeGenerator?.newNumber()
        salesTrx.status = SalesStatus.ACTIVE.status
        salesTrx.totalSalesAmount = totalAmount
        salesTrx.salesList = mutableSetOf(sales)
        return salesTrxDao?.save(salesTrx)!!
    }

    fun persistSales(product: Product) : Sales {
        var sales = Sales()
        sales.product = product
//        sales.salesTrx = salesTrx
        sales.status = SalesStatus.ACTIVE.status
        sales.salesAmount = product.price
        return salesDao?.save(sales)!!
    }

    fun persistSalesTrxLog(logMsg: String, salesTrx: SalesTrx) : SalesTrxLog {
        var salesTrxLog = SalesTrxLog()
        salesTrxLog.description = logMsg
        salesTrxLog.salesTrx = salesTrx
        salesTrxLog.status = SalesStatus.ACTIVE.status
        return salesTrxLogDao?.save(salesTrxLog)!!
    }

    fun getUserDetail() : UserDetails {
        return SecurityContextHolder.getContext().authentication.principal as UserDetails
    }

    fun getSalesTrxByUser(user: User) : MutableList<SalesTrx>? {
        return salesTrxDao?.findByUser(user)

    }

//    fun getSalesBySalesTrx(salesTrx: SalesTrx) : MutableList<Sales>? {
//        return salesDao?.findAllBySalesTrx(salesTrx)
//    }

    fun getUser(userId: Long) : User? {
        return userDao?.findOne(userId)
    }

    fun getProduct(productId: Long) : Product? {
        return productDao?.findOne(productId)
    }

    fun calcTotalAmount(productList : MutableList<Product>) : BigDecimal {
        var totalAmount = BigDecimal.ZERO
        productList.forEach {
            product ->
            totalAmount = totalAmount.add(product.price)
        }

        return totalAmount
    }

    fun validateArgs(grantProductFormDto: GrantProductFormDto, user: User?, product: Product?) {
        if (user == null) {
            val errMsg = "[User ID- " + grantProductFormDto.userId + "]not found"
            log.error(errMsg)
            throw ValidationException("userId", "", errMsg)
        }
        if (product == null) {
            val errMsg = "[Product ID- " + grantProductFormDto.productId + "]not found"
            log.error(errMsg)
            throw ValidationException("productId", "", errMsg)
        }

        // Check user have owned the requested product
        val salesTrxList = getSalesTrxByUser(user)
        if (salesTrxList == null) {
            // No user related transaction found
            return
        } else {

            salesTrxList.forEach { salesTrx ->

                run {
                    // Get Sale record by Sale Trx
                    val saleList = salesTrx.salesList

                    if (saleList != null && saleList.isNotEmpty()) {

                        saleList.forEach { sale ->
                            run {

//                                log.info("${sale.product!!.id} --- ${grantProductFormDto.productId} : ${sale.product!!.id === grantProductFormDto.productId}")
                                log.info("Product Status - [${sale.product!!.status}] --- Sales Status - [${SalesStatus.ACTIVE.status}] : ${sale.product!!.id!! == grantProductFormDto.productId}")

                                if (sale.product != null
                                        && sale.product!!.id == grantProductFormDto.productId
                                    && sale.status == SalesStatus.ACTIVE.status
                                ) {
                                    log.error("Exception thrown")
                                    throw ValidationException("productId", "", "User owned this product already")
                                }

                            }
                        }

                    } else {
                        throw IllegalArgumentException("Sales Trx not found")
                    }

                }

            }


        }


    }

}
