package com.samaxxw.service.impl

import com.samaxxw.dto.io.FileUploadDto
import com.samaxxw.model.FileAttachment
import com.samaxxw.service.FileDownloadService
import com.samaxxw.service.FileUploadService
import com.samaxxw.service.GoogleFileUploadService
import com.samaxxw.service.generator.ReferenceCodeGenerator
import com.samaxxw.util.ExportTypeEnum
import com.samaxxw.util.MimeTypeConstants
import org.apache.commons.codec.binary.Base64
import org.apache.commons.codec.binary.StringUtils
import org.apache.http.entity.ContentType
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

@Service
class ExportLogFileAttachmentService {

//    @Autowired
//    private val fileUploadService: FileUploadService? = null

    @Autowired
    private val fileUploadService: GoogleFileUploadService? = null

    @Autowired
    private val fileDownloadService: FileDownloadService? = null

//    @Value("\${minio.default-bucket}")
    @Value("\${google.root-folder-name}")
    val bucket: String? = null

    @Autowired
    val referenceCodeGenerator: ReferenceCodeGenerator? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
    }

    fun save(fileName: String, type: ExportTypeEnum, source: ByteArray, contentType: ContentType, purpose: String): FileAttachment {

        var fileUploadDto = createFileUpdateDto(fileName, type, source, contentType, purpose)
        val fileAttachment = fileUploadService?.execute(fileUploadDto)
        return fileAttachment!!

    }

    fun createFileUpdateDto(fileName: String, type: ExportTypeEnum, source: ByteArray, contentType: ContentType, purpose: String): FileUploadDto {

        val path = "export-log/${type.type}/"
        val fileUploadDto: FileUploadDto = FileUploadDto()
        fileUploadDto.uploadId = UUID.randomUUID().toString() // will be replaced by google file id after upload
        fileUploadDto.bucket = bucket
        fileUploadDto.contentType = contentType.toString()
        fileUploadDto.encodedFileFormat = StringUtils.newStringUtf8(Base64.encodeBase64(source, false))
        fileUploadDto.objectName = path + referenceCodeGenerator?.newNumber() + '_' + fileName
        fileUploadDto.fileName = fileName + MimeTypeConstants.getExtension(contentType.mimeType)
        fileUploadDto.purpose = purpose
        fileUploadDto.groupName = "export-log"
        return fileUploadDto

    }


}