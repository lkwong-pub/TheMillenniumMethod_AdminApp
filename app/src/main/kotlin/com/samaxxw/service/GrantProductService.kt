package com.samaxxw.service

import com.samaxxw.dto.form.product.ProductEditFormDto
import com.samaxxw.dto.form.product.ProductFormDto
import com.samaxxw.dto.form.user.product.GrantProductFormDto
import com.samaxxw.model.Product
import com.samaxxw.model.User

interface GrantProductService {

    fun grantProduct(grantProductFormDto: GrantProductFormDto)

}
