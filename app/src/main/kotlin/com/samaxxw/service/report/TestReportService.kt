package com.samaxxw.service.report

interface TestReportService {

    fun generateReport(): String

}