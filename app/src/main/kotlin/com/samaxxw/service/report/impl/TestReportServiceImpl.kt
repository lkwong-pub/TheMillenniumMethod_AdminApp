package com.samaxxw.service.report.impl

import com.samaxxw.dao.UserDao
import com.samaxxw.model.User
import com.samaxxw.service.report.TestReportService
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import java.util.*

@Service
class TestReportServiceImpl : TestReportService {

    @Autowired
    val userDao: UserDao? = null

    fun getUserList(): List<User> {
        return userDao?.findAll()!!
    }


    private val empList = Arrays.asList<Employee>(
            Employee(1, "Sandeep", "Data Matrix", "Front-end Developer", 20000),
            Employee(2, "Prince", "Genpact", "Consultant", 40000),
            Employee(3, "Gaurav", "Silver Touch ", "Sr. Java Engineer", 47000),
            Employee(4, "Abhinav", "Akal Info Sys", "CTO", 700000))

    override fun generateReport(): String {
        try {

            val reportPath = "resources"

            val file = ClassPathResource("report/user-rpt.jrxml").file

            // Compile the Jasper report from .jrxml to .japser
            //			JasperReport jasperReport = JasperCompileManager.compileReport(reportPath + "/employee-rpt.jrxml");
            val jasperReport = JasperCompileManager.compileReport(file.path)

            // Get your data source
            val jrBeanCollectionDataSource = JRBeanCollectionDataSource(getUserList())

            // Add parameters
            val parameters = HashMap<String, Any>()

            parameters["createdBy"] = "Websparrow.org"
            parameters["reportTitle"] = "Test title"


            // Fill the report
            val jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
                    jrBeanCollectionDataSource)

            // Export the report to a PDF file
            JasperExportManager.exportReportToPdfFile(jasperPrint, "MM-Test-Report.pdf")

            println("Done")

            return "Report successfully generated @path= $reportPath"

        } catch (e: Exception) {
            e.printStackTrace()
            return e.message!!
        }

    }


    class Employee(var id: Int, var name: String?, var oraganization: String?, var designation: String?, var salary: Int)


}