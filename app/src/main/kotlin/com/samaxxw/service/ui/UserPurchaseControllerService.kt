package com.samaxxw.service.ui

import com.samaxxw.dao.SalesTrxDao
import com.samaxxw.dto.model.SalesTrxLogDto
import com.samaxxw.dto.paged.PagedDto
import com.samaxxw.dto.search.BaseSearchRequest
import com.samaxxw.dto.search.SalesTrxLogSearchRequest
import com.samaxxw.model.SalesTrx
import com.samaxxw.service.impl.SalesTrxLogTrxService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class UserPurchaseControllerService {

    @Autowired
    val salesTrxDao: SalesTrxDao? = null

    @Autowired
    val salesTrxLogTrxService: SalesTrxLogTrxService? = null

    fun getSalesTrxLog(salesTrxId: Long, searchRequest: BaseSearchRequest) : PagedDto<SalesTrxLogDto> {
        return salesTrxLogTrxService?.searchBy(createSalesTrxLogSearchRequest(salesTrxId, searchRequest))!!
    }

    fun createSalesTrxLogSearchRequest(salesTrxId: Long, searchRequest: BaseSearchRequest) : SalesTrxLogSearchRequest {

        var request = SalesTrxLogSearchRequest()
        request.salesTrx = getSalesTrx(salesTrxId)
        request.pageNum = searchRequest.pageNum
        request.pageSize = searchRequest.pageSize

        return request

    }

    fun getSalesTrx(id: Long) : SalesTrx {
        return salesTrxDao?.findOne(id)!!
    }
}