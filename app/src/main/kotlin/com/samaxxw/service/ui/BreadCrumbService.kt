package com.samaxxw.service.ui

import com.samaxxw.controller.*
import com.samaxxw.controller.template.BreadcrumbListDto
import com.samaxxw.dto.form.pageResources.PageResourcesSectionEnum
import org.springframework.stereotype.Service

@Service
class BreadCrumbService : AbstractBreadCrumbService() {

    /**
     * User Module
     */

    fun getUserRootBreadCrumb() : BreadcrumbListDto {
        return getUserRootBreadCrumb(false)
    }

    fun getUserRootBreadCrumb(isCurrentPage : Boolean) : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("User", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("User", "${UserController.ROOT}/list", false))
        }
        return breadcrumb
    }

    fun getUserAddBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getUserRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Add User", null, true))
        return breadcrumb
    }

    fun getUserEditBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getUserRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Edit User", null, true))
        return breadcrumb
    }

    fun getUserPurchaseRecordRootBreadCrumb(userId: Long?, isCurrentPage: Boolean) : BreadcrumbListDto {
        val breadcrumb = getUserRootBreadCrumb()
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("User Purchase Record", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("User Purchase Record", "${UserPurchaseController.ROOT}/user/$userId/list", false))
        }
        return breadcrumb
    }

    fun getUserPurchaseRecordRootBreadCrumb() : BreadcrumbListDto {
        return getUserPurchaseRecordRootBreadCrumb(null, false)
    }

    fun getUserPurchaseRecordBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getUserRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("User Purchase Record", null, true))
        return breadcrumb
    }

    fun getUserGrantProductBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getUserRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("User Grant Product", null, true))
        return breadcrumb
    }

    /**
     * Product Module
     */

    fun getProductRootBreadCrumb() : BreadcrumbListDto {
        return getProductRootBreadCrumb(false)
    }
    fun getProductRootBreadCrumb(isCurrentPage: Boolean) : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product", "${ProductController.ROOT}/list", false))
        }
        return breadcrumb
    }

    fun getProductAddBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getProductRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Add Product", null, true))
        return breadcrumb
    }

    fun getProductEditBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getProductRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Edit Product", null, true))
        return breadcrumb
    }

    /**
     * Product Resources Module
     */

    fun getProductResourcesRootBreadCrumb(productId : Long) : BreadcrumbListDto {
        return getProductResourcesRootBreadCrumb(productId, false)
    }

    fun getProductResourcesRootBreadCrumb(productId : Long, isCurrentPage: Boolean) : BreadcrumbListDto {
        val breadcrumb = getProductRootBreadCrumb()

        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product Resources", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product Resources - $productId", "${ProductResourcesController.ROOT}/product/$productId/list", false))
        }

        return breadcrumb
    }

    fun getProductResourcesAddBreadCrumb(productId : Long) : BreadcrumbListDto {
        val breadcrumb = getProductResourcesRootBreadCrumb(productId)
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product Resources Add", null, true))
        return breadcrumb
    }

    fun getProductResourcesEditBreadCrumb(productId : Long) : BreadcrumbListDto {
        val breadcrumb = getProductResourcesRootBreadCrumb(productId)
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product Resources Edit", null, true))
        return breadcrumb
    }

    /**
     * Product Sales Module
     */

    fun getProductSalesRootBreadCrumb(productId : Long) : BreadcrumbListDto {
        return getProductSalesRootBreadCrumb(productId, false)
    }

    fun getProductSalesRootBreadCrumb(productId : Long, isCurrentPage: Boolean) : BreadcrumbListDto {
        val breadcrumb = getProductRootBreadCrumb()

        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product User Sales Record", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product User Sales Record - $productId", "${ProductController.ROOT}/$productId/user/list", false))
        }

        return breadcrumb
    }

    /**
     * Page Resources Module
     */
    fun getPageResourcesRootBreadCrumb() : BreadcrumbListDto {
        return getPageResourcesRootBreadCrumb(false)
    }

    fun getPageResourcesRootBreadCrumb(isCurrentPage : Boolean) : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources", "${PageResourcesController.ROOT}/list", false))
        }
        return breadcrumb
    }

    fun getPageResourcesAddBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getPageResourcesRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources Add", null, true))
        return breadcrumb
    }


    fun getPageResourcesEditBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getPageResourcesRootBreadCrumb()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources Edit", null, true))
        return breadcrumb
    }

    fun getPageResourcesSectionListBreadCrumb(isCurrentPage : Boolean) : BreadcrumbListDto {
        val breadcrumb = getPageResourcesRootBreadCrumb()
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources - Section List", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources - Section List", "${PageResourcesController.ROOT}/section/list", false))
        }
        return breadcrumb
    }

    fun getPageResourcesSectionItemListBreadCrumb(section: String) : BreadcrumbListDto {
        val breadcrumb = getPageResourcesSectionListBreadCrumb(false)
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources - Section $section", null, true))
        return breadcrumb
    }

    fun getPageResourcesSectionItemListBreadCrumb(section: PageResourcesSectionEnum, isCurrentPage: Boolean) : BreadcrumbListDto {
        val breadcrumb = getPageResourcesSectionListBreadCrumb(false)
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources - Section ${section.displayName}", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources - Section ${section.displayName}", "${PageResourcesController.ROOT}/section/${section.name}/list", false))

        }
        return breadcrumb
    }


    fun getPageResourcesAddFromSectionItemListBreadCrumb(section: PageResourcesSectionEnum) : BreadcrumbListDto {
        val breadcrumb = getPageResourcesSectionItemListBreadCrumb(section, false)
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources Add", null, true))
        return breadcrumb
    }

    fun getPageResourcesEditFromSectionItemListBreadCrumb(section: PageResourcesSectionEnum) : BreadcrumbListDto {
        val breadcrumb = getPageResourcesSectionItemListBreadCrumb(section, false)
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Page Resources Edit", null, true))
        return breadcrumb
    }

    fun getSalesTrxStatusChangeBreadCrumb(userId: Long) : BreadcrumbListDto {
        val breadcrumb = getUserPurchaseRecordRootBreadCrumb(userId, false)
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Update Sales Transaction Status", null, true))
        return breadcrumb
    }
}