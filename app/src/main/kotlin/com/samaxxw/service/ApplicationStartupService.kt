package com.samaxxw.service

import com.samaxxw.service.impl.ExportLogService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component


@Component
class ApplicationStartupService : ApplicationRunner {

    @Autowired
    val exportLogService: ExportLogService? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
    }

    @Throws(Exception::class)
    override fun run(applicationArguments: ApplicationArguments) {
        log.info("*** Start up runner ***")
        exportLogService?.cleanTimeoutTask()
        // check all time-out export task
    }
}