package com.samaxxw.interceptor


import com.samaxxw.model.Permission
import com.samaxxw.model.Role
import com.samaxxw.model.RolePermissionMap
import com.samaxxw.service.RolePermissionMapService
import com.samaxxw.service.RoleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.lang.Nullable
import org.springframework.security.core.GrantedAuthority
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class SecurityInterceptor : HandlerInterceptorAdapter() {

    @Autowired
    private val rolePermissionMapService : RolePermissionMapService? = null

    @Autowired
    private val roleService : RoleService? = null

    @Throws(Exception::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val startTime = System.currentTimeMillis()
        println("\n-------- LogInterception.preHandle --- ")
        println("Request URL: " + request.requestURL)
        println("Start Time: " + System.currentTimeMillis())

        request.setAttribute("startTime", startTime)

        // ## Security Header Check begin ##

        val handlerResult = true
        val requestURI = request.servletPath
        val logPrefix = "FROM : " + request.remoteAddr + "- preHandle(): "
        val requestPermission = Permission()
        val session = request.session

        // User Security context
        val authentication = SecurityContextHolder.getContext().authentication
        val authorities : MutableCollection<out GrantedAuthority>? = authentication.authorities


        requestPermission.url = requestURI

        // TODO check with override user role like ROLE_ADMIN
        // Get current role permission map
        // Loop current user authorities
        authorities!!.forEach { authority ->

            if ("ROLE_DISABLED" == authority.authority) {
                if (requestURI == "/") {
                    response.sendRedirect(request.contextPath + "/login") // Redirect to access denied page
                } else {
                    response.sendRedirect(request.contextPath + "/accessDenied") // Redirect to access denied page
                }
                return false
            }

            if ("ROLE_ADMIN" == authority.authority) {
                return true
            }

            val role : Role? = roleService!!.findByName(authority.authority)
            val rolePermissionList : MutableList<RolePermissionMap> = rolePermissionMapService!!.findByRole(role)!!

            rolePermissionList.forEach { rolePermission ->

                println(rolePermission.permission!!.url)
                if (requestURI.matches(Regex(rolePermission.permission!!.url!!))) {
                    return true
                }
            }

        }

        response.sendRedirect(request.contextPath + "/accessDenied") // Redirect to access denied page

        return false



//        val rolePermissionList : MutableList<RolePermissionMap> = rolePermissionMapService.findByRole(a)

//        println(requestPermission)
//
//        return true
    }

    @Throws(Exception::class)
    override fun postHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any, @Nullable modelAndView: ModelAndView?) {

        println("\n-------- LogInterception.postHandle --- ")
        println("Request URL: " + request.requestURL)

        // You can add attributes in the modelAndView
        // and use that in the view page
    }

    @Throws(Exception::class)
    override fun afterCompletion(request: HttpServletRequest, response: HttpServletResponse, handler: Any, @Nullable ex: java.lang.Exception?) {
        println("\n-------- LogInterception.afterCompletion --- ")

        val startTime = request.getAttribute("startTime") as Long
        val endTime = System.currentTimeMillis()
        println("Request URL: " + request.requestURL)
        println("End Time: $endTime")

        println("Time Taken: " + (endTime - startTime))
    }

}