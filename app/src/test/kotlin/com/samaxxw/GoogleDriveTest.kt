package com.samaxxw

import com.samaxxw.service.impl.GoogleDriveClientService
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.ClassPathResource
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.util.*

@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
@Ignore
class GoogleDriveTest {

    @Autowired
    val googleDriveService: GoogleDriveClientService? = null

    @Test
    @Throws(Exception::class)
    fun testListFile() {
        googleDriveService?.getFileList("MillenniumMethod_Admin_Dev")
    }

    @Test
    @Throws(Exception::class)
    fun testPrintAbout() {
        googleDriveService?.printAbout()
    }

    @Test
    @Throws(Exception::class)
    fun testUpload() {
        googleDriveService?.upload()
    }

    @Test
    @Throws(Exception::class)
    fun testCreateFolder() {
        googleDriveService?.createBucket("TheMillenniumMethod_DEV")
    }

    @Test
    @Throws(Exception::class)
    fun testShare() {
        googleDriveService?.share("pubmaxwong@gmail.com", "1Lcnet7r2OdZNSvneTHYPpGMne_G4LcGQ")
    }

    @Test
    @Throws(Exception::class)
    fun testDelete() {
        val file = googleDriveService?.getFileBySubFolderAndFileName("Product1", "4087eade-7cf0-41c9-a9bc-e57562c5ba4f.jpg")
        googleDriveService?.deleteFile(file!!.id)
    }

    @Test
    @Throws(Exception::class)
    fun testDeleteFolder() {
        googleDriveService?.deleteFile("1Lcnet7r2OdZNSvneTHYPpGMne_G4LcGQ")
    }

    @Test
    @Throws(Exception::class)
    fun testGetFileStat() {
        val file = googleDriveService?.getFileBySubFolderAndFileName("Product1", "9efe72c7-0ea2-4906-9bc6-c20bed253f54.jpg")
        val fileStat = googleDriveService?.getFileStat(file!!.id)
        println(fileStat)
    }

    @Test
    @Throws(Exception::class)
    fun test_CheckFolderExistingOnROot() {
        googleDriveService?.checkBucketExisted("Test")
    }

    @Test
    @Throws(Exception::class)
    fun test_create_subfolder_as_new_product() {
        googleDriveService?.createSubFolder("TheMillenniumMethod_DEV", "Product2")
    }

    @Test
    @Throws(Exception::class)
    fun test_get_subfolder_list() {
        googleDriveService?.getBucketFolderList()
    }

    @Test
    @Throws(Exception::class)
    fun testGetAllFolders() {
        googleDriveService?.getAllFolderList()
    }

    @Test
    @Throws(Exception::class)
    fun test_upload_file_to_subfolder() {
        val filePath = ClassPathResource("file/1_9UyG2rWcTHJmjdB5-SN55Q.jpeg").file
        googleDriveService?.uploadToFolder("TheMillenniumMethod_DEV", "Product3", "image/jpeg", "${UUID.randomUUID()}.jpg", filePath.inputStream().readBytes())
    }

    @Test
    @Throws(Exception::class)
    fun resetGoogleDrive() {
        val files = googleDriveService?.getAllFolderList()
        files!!.forEach {  googleDriveService?.deleteFile(it.id) }
    }
}
