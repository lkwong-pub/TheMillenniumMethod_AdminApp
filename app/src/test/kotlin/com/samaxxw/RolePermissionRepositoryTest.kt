package com.samaxxw

import com.samaxxw.model.Role
import com.samaxxw.model.RolePermissionMap
import com.samaxxw.repository.RolePermissionMapRepository
import com.samaxxw.repository.RoleRepository
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner


@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
class RolePermissionRepositoryTest {

    @Autowired
    private val roleRepository: RoleRepository? = null

    @Autowired
    private val rolePermissionRepository: RolePermissionMapRepository? = null

    @Test
    fun test_findByRoleName_expected_success() {

        val role : Role? = roleRepository!!.findByName("ROLE_USER")

        val result : MutableList<RolePermissionMap> = rolePermissionRepository!!.findByRole(role)
        print(result)
        Assert.assertNotNull(result)
    }
}