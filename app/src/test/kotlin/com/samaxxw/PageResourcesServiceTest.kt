package com.samaxxw

import com.samaxxw.dto.form.pageResources.PageResourcesSectionEnum
import com.samaxxw.dtoservice.PageResourcesDtoService
import com.samaxxw.mongo.dao.PageResourcesDao
import com.samaxxw.mongo.service.PageResourcesService
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional
import org.thymeleaf.util.StringUtils
import java.lang.NumberFormatException
import java.util.*


@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
class PageResourcesServiceTest {

    @Autowired
    private val pageResourcesDtoService: PageResourcesDtoService? = null

    @Autowired
    private val pageResourcesDao: PageResourcesDao? = null

    @Test
    @Transactional
    fun test_getSectionItemList_expectedSuccess() {

        val pageResourcesSectionItemResponse = pageResourcesDao?.findAllBySection("l14tdt")

        pageResourcesSectionItemResponse!!.forEach {

            val name = StringUtils.replace(it.name, "p", "")

            val segment = listOf(name.trim().split("_"))

            val num: List<String>? = segment.elementAt(0)

//            println("${num!![0]}  ${num.size}")

            try {
                var numInt = num!![0].toInt()

                if (numInt in 2..35) {

                    if (num.size > 1) {
                        println("oldVal : p${numInt}_${num[1]}")
                    } else {
                        println("oldVal : p${numInt}")
                    }

                    numInt -= 1

                    var newVal = ""
                    if (num.size > 1) {
                        newVal = "p${numInt}_${num[1]}"
                    } else {
                        newVal = "p${numInt}"
                    }

                    it.name = newVal
                    println("newVal : ${it.name}")
//                    pageResourcesDao!!.save(it)

                }
            } catch (nfe: NumberFormatException) {
                println("skipped non handled record")
            }

        }
        Assert.assertNotNull(pageResourcesSectionItemResponse)

    }

    @Test
    @Transactional
    fun test_renameSection() {
        val pageResourcesSectionItemResponse = pageResourcesDao?.findAllBySection("l6t2")


        pageResourcesSectionItemResponse!!.forEach {

            it.section = PageResourcesSectionEnum.S6.persistName
            println(it.name)
//            pageResourcesDao!!.save(it)

        }

    }


}