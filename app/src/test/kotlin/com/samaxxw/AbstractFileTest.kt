package com.samaxxw

import java.io.File
import java.nio.file.Paths

open class AbstractFileTest {
    protected fun getSampleFileByRelative(subPackage: String, targetFile: String): File {

        val c = this.javaClass
        val path = c.getResource(c.getSimpleName() + ".class").getPath().replace(c.getSimpleName() + ".class", "")

        val i = path.indexOf(subPackage)
        val packagePath = path.substring(0, i + subPackage.length)

        val p = Paths.get(packagePath, "/src/test/resources/file/", targetFile)
        return p.toFile()
    }
}