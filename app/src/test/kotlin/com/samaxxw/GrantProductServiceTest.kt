package com.samaxxw

import com.samaxxw.dto.form.user.product.GrantProductFormDto
import com.samaxxw.exception.ValidationException
import com.samaxxw.model.QRole.role
import com.samaxxw.model.Role
import com.samaxxw.model.RolePermissionMap
import com.samaxxw.model.SalesTrx
import com.samaxxw.repository.RolePermissionMapRepository
import com.samaxxw.repository.RoleRepository
import com.samaxxw.repository.SalesTrxRepository
import com.samaxxw.service.GrantProductService
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner


@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
@Ignore
class GrantProductServiceTest {

    @Autowired
    private val grantProductService: GrantProductService? = null

    @Test
    fun test_grantProductToUser_expected_success() {

        val grantProductFormDto = GrantProductFormDto()
        grantProductFormDto.productId = 1
        grantProductFormDto.userId = 1
        grantProductService?.grantProduct(grantProductFormDto)
        Assert.assertTrue(true)

    }

    @Test
    fun test_grantProductToUser_expected_failed() {

        try {
            val grantProductFormDto = GrantProductFormDto()
            grantProductFormDto.productId = 1
            grantProductFormDto.userId = 202
            grantProductService?.grantProduct(grantProductFormDto)
        } catch (ve : ValidationException) {
            Assert.assertTrue(true)
        } catch (e : Exception) {
            Assert.assertTrue(false)
        }

    }
}