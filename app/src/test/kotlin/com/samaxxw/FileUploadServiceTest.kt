package com.samaxxw

import com.samaxxw.dto.io.FileUploadDto
import com.samaxxw.service.FileUploadService
import com.samaxxw.service.MinioS3ClientService
import com.samaxxw.util.AttachmentUtils
import io.minio.MinioClient
import junit.framework.Assert.assertNotNull
import org.apache.commons.lang3.RandomStringUtils
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional
import java.io.IOException


@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration
class FileUploadServiceTest : AbstractFileTest() {

    @Autowired
    val fileUploadService: FileUploadService? = null

    @Autowired
    val minioS3ClientService : MinioS3ClientService? = null

//    companion object {
//        @BeforeClass
//        fun beforeClass() {
//            System.setProperty("minio.url", "http://localhost:9000")
//        }
//    }


    @Test
    @Transactional
    @Throws(Exception::class)
    fun testWriteFile() {
        writeFileAndAssert()
    }

    @Throws(IOException::class)
    protected fun writeFileAndAssert() {

        val client: MinioClient = minioS3ClientService?.getClient()!!
        val f = getSampleFileByRelative("app", "1_9UyG2rWcTHJmjdB5-SN55Q.jpeg")
        val fileName = f.getName()

        val encodedFormat = AttachmentUtils.toBase64String(f)

        val ioContext = FileUploadDto()
        ioContext.bucket = "millennium-method-dev"
        ioContext.uploadId = "millennium-method-resource-${RandomStringUtils.randomNumeric(5)}"
        ioContext.objectName = ("/resources/" + RandomStringUtils.randomNumeric(5) + "/" + fileName)
        ioContext.fileName = fileName
        ioContext.encodedFileFormat = encodedFormat

        val fileAttachment = fileUploadService?.execute(ioContext)
        assertNotNull(fileAttachment)
    }

}