package com.samaxxw

import com.samaxxw.model.QRole.role
import com.samaxxw.model.Role
import com.samaxxw.model.RolePermissionMap
import com.samaxxw.model.SalesTrx
import com.samaxxw.repository.RolePermissionMapRepository
import com.samaxxw.repository.RoleRepository
import com.samaxxw.repository.SalesTrxRepository
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner


@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
class SalesTrxRepositoryTest {

    @Autowired
    private val saleTrxRepository: SalesTrxRepository? = null



    @Test
    fun test_findAll_getSaleList_expected_success() {

        val list : MutableList<SalesTrx>? = saleTrxRepository?.findAll()

        if (list != null && list.isNotEmpty()) {
            list.forEach {
                item ->
                 Assert.assertNotNull(item.salesList)
            }

        }
    }
}