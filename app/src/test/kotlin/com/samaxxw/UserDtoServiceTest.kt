package com.samaxxw

import com.samaxxw.dto.model.UserDto
import com.samaxxw.dto.paged.PagedDto
import com.samaxxw.dtoservice.UserDtoService
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageRequest
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner


@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
class UserDtoServiceTest : Mockito() {

    val PAGE_PARAM = "page"
    val PER_PAGE = "per_page"
    val SORT_PARAM = "sort"
    val RESET_FLAG = "false"

    @Autowired
    private val userDtoService: UserDtoService? = null

    @Test
    fun test_findAll_expected_success() {

        val userList : List<UserDto>? = userDtoService!!.findAll()
        userList!!.forEach { println("E-mail : " + it.email) }
        Assert.assertNotNull(userList)

    }

    @Test
    fun test_findBySpec_expected_success() {

        val request = MockHttpServletRequest()
        request.setAttribute("first_name", "Admin62981")
        request.queryString = "first_name=&last_name=&email="

        val pageable = PageRequest.of(0, 20)
        val sortField = "first_name"

        // TODO use base controller findAllWithSpec
        val userList : PagedDto<UserDto>? = userDtoService!!.findAllWithSpec(pageable, PAGE_PARAM, PER_PAGE, SORT_PARAM, sortField, request)
        userList!!.content!!.forEach { println("E-mail : " + it.email) }
        Assert.assertNotNull(userList)

    }
}