GIT_VERSION=$(git rev-parse --verify --short HEAD)
docker tag millennium-method/admin:latest 278131614032.dkr.ecr.us-east-2.amazonaws.com/the-millennium-resources-server-admin-uat:$GIT_VERSION
$(aws ecr get-login --no-include-email)
docker push 278131614032.dkr.ecr.us-east-2.amazonaws.com/the-millennium-resources-server-admin-uat:$GIT_VERSION
echo Latest tag : $GIT_VERSION